# No Name Project Android

## Description
Application qui recense les films sortis et les prochaines sorties.
L'utilisateur se connectera avec un Google Account.
Il pourra notifier les films qu'il souhaite regardé

## Url API

URL de base: https://developers.themoviedb.org/3/

Method type | URL | Parameters | Headers | Type Headers |
--- | --- | --- | --- | --- |
GET | /genre/movie/list | none | api_key | string
GET | /movie/{movie_id} | id (long) | api_key | string
GET | /movie/popular | none | api_key | string
GET | /movie/upcoming | none | api_key | string


## Todo

```
    - Gestion de comptes:
        * Google SignIn
        * Google SignOut
    
    - Mettre retrofit dans la lib en mode Asynchrone
    
    - finir le main
    
    - splashscreen
    
    - liste d'icone des categories
    
    - SettingsActivity:
        * deconnexion google
        * synchonisation faribase (upload et download)
    
    - Ajouter un rappel dans le calendrier avec la de sortie des prochains films à voir (+++)
```

## History on develop

- Mar 01/05/2018

```
    - API de recherche
        * par nom
        * par année (abbandonner)
        
    - L'activity de details de movie prend un id en param
```

- Ven 27/04/2018

```
    - ListMoviesActivity:
        * Add 2 request web sevices (GET popular & GET up comming)
        * manage input to select request
        * put id movie in param for DetailsMovieAcitvity

    - add splashscreen temp to delocate category singleton
```

- Mar 24/24/2018

```
    - MainActivity:
        * UI
        * Link SQLite

    - ListMoviesActivity:
        * UI
        * Request Web Sevices (GET all category)
        * Request Web Sevices (GET all movies)

    - DetailsMovieAcitvity:
        * UI
        * Request Web Services (GET one movie)
        * Request Web Services (GET all similare movies)

    - Add SQLite DataBase: for My Library

    - Search bar at top
```
