package com.nicoberangerenico.moviesapp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;
import com.nicoberangerenico.moviesapp.tools.ConstFile;
import com.nicoberangerenico.movieslib.Models.Movie;
import com.nicoberangerenico.movieslib.SQLite.DatabaseAccess;

import java.util.List;

public class FirebaseManagement {

    /**
     * Download all movies from firebase
     * @param context context
     * @param user_id id of the user
     */
    public static void downloadDatabase(final Context context, String user_id, final IFirebaseListener listener){
        DatabaseReference database = FirebaseDatabase.getInstance().getReference();

        database.child(user_id).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if(dataSnapshot.hasChild(ConstFile.FIREBASE_NODE_MOVIE)){
                    List<Movie> movieList =
                            dataSnapshot.child(ConstFile.FIREBASE_NODE_MOVIE).getValue(new GenericTypeIndicator<List<Movie>>(){});

                    if(movieList != null){
                        DatabaseAccess databaseAccess = DatabaseAccess.getInstance(context);

                        for (Movie movie: movieList) {
                            if(!databaseAccess.movieExist(movie.getId())){
                                databaseAccess.insertMovie(movie);
                            }
                        }

                        listener.onDataChange();

                        Toast.makeText(context, R.string.firebase_download_success, Toast.LENGTH_SHORT).show();
                        return;
                    }
                }

                Toast.makeText(context, R.string.firebase_download_no_data, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(context, R.string.firebase_download_cancel, Toast.LENGTH_SHORT).show();
            }
        });
    }


    /**
     * Save all movies in firebase
     * @param context context
     * @param user_Id id of the user
     */
    public static void uploadDatabase(final Context context, String user_Id, final IFirebaseListener listener) {
        FirebaseDatabase.getInstance().getReference()
                .child(user_Id).child(ConstFile.FIREBASE_NODE_MOVIE)
                .setValue(DatabaseAccess.getInstance(context).getListMovies())
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        listener.onDataChange();
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(context, R.string.firebase_upload_error, Toast.LENGTH_SHORT).show();
                    }
                });
    }
}
