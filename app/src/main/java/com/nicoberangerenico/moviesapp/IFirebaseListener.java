package com.nicoberangerenico.moviesapp;

public interface IFirebaseListener {
    void onDataChange();
}
