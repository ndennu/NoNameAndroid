package com.nicoberangerenico.moviesapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.nicoberangerenico.moviesapp.R;
import com.nicoberangerenico.moviesapp.adapter.GenreAdapter;
import com.nicoberangerenico.movieslib.Models.Genre;
import com.nicoberangerenico.movieslib.SQLite.DatabaseAccess;

import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GenreActivity extends AppCompatActivity {

    @BindView(R.id.recyclerViewGenre) RecyclerView recyclerViewGenre;

    private DatabaseAccess databaseAccess;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_genre);

        Objects.requireNonNull(getSupportActionBar()).setHomeButtonEnabled(true);

        ButterKnife.bind(this);

        databaseAccess = DatabaseAccess.getInstance(this);

        InitializeUI();
    }

    private void InitializeUI() {
        recyclerViewGenre.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewGenre.setItemAnimator(new DefaultItemAnimator());

        List<Genre> genreList = databaseAccess.getAllGenre();

        GenreAdapter genreAdapter = new GenreAdapter(this, genreList);
        genreAdapter.setListener(new GenreAdapter.Listener() {
            @Override
            public void onGenreClick(Genre genre) {
                Intent intent = new Intent(GenreActivity.this, MoviesActivity.class);
                intent.putExtra(MoviesActivity.CATEGORY_PARAM, genre);
                startActivity(intent);
            }
        });
        recyclerViewGenre.setAdapter(genreAdapter);
    }
}
