package com.nicoberangerenico.moviesapp.activity;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.miguelcatalan.materialsearchview.MaterialSearchView;
import com.nicoberangerenico.moviesapp.R;
import com.nicoberangerenico.moviesapp.adapter.MovieLibraryAdapter;
import com.nicoberangerenico.movieslib.Models.Movie;
import com.nicoberangerenico.movieslib.SQLite.DatabaseAccess;

import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LibraryActivity extends AppCompatActivity {

    @BindView(R.id.recyclerView) RecyclerView recyclerView;
    @BindView(R.id.txt_no_result) TextView txtNoResult;
    @BindView(R.id.search_view) MaterialSearchView searchView;
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.swipeRefreshLayout) SwipeRefreshLayout swipeRefreshLayout;

    private List<Movie> movieList;
    private MovieLibraryAdapter movieLibraryAdapter;
    private DatabaseAccess databaseAccess;

    private int recyclerViewWidthCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_library);

        setTitle(R.string.my_library);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        databaseAccess = DatabaseAccess.getInstance(this);

        InitializeUI();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_menu, menu);

        MenuItem item = menu.findItem(R.id.action_search);
        searchView.setMenuItem(item);

        return true;
    }

    @Override
    public void onBackPressed() {
        if (searchView.isSearchOpen()) {
            searchView.closeSearch();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            recyclerViewWidthCount = 5;
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            recyclerViewWidthCount = 3;
        }
    }

    private void InitializeUI() {
        //SwipeRefreshLayout Initialization
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                movieList.clear();
                movieList.addAll(databaseAccess.getListMovies());

                if (movieList.size() == 0) {
                    txtNoResult.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                }

                movieLibraryAdapter.notifyDataSetChanged();
                swipeRefreshLayout.setRefreshing(false);
            }
        });
        swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);

        //SearchView Initialization
        searchView.setVoiceSearch(false);
        searchView.setCursorDrawable(R.drawable.color_cursor_white);
        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                movieList.clear();
                movieList.addAll(databaseAccess.searchMovieByTitle(query));

                txtNoResult.setVisibility(movieList.size() > 0 ? View.GONE : View.VISIBLE);
                recyclerView.setVisibility(movieList.size() > 0 ? View.VISIBLE : View.GONE);
                movieLibraryAdapter.notifyDataSetChanged();

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        //RecyclerView Initialization
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            recyclerViewWidthCount = 3;
        } else {
            recyclerViewWidthCount = 5;
        }

        recyclerView.setLayoutManager(new GridLayoutManager(this, recyclerViewWidthCount));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        movieList = databaseAccess.getListMovies();

        if (movieList.size() == 0) {
            txtNoResult.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }

        movieLibraryAdapter = new MovieLibraryAdapter(this, movieList);
        movieLibraryAdapter.setListener(new MovieLibraryAdapter.Listener() {
            @Override
            public void onMovieClick(Movie movie) {
                Intent intent = new Intent(LibraryActivity.this, MovieDetailsActivity.class);
                intent.putExtra(MovieDetailsActivity.MOVIE_PARAM, movie);
                startActivity(intent);
            }
        });

        recyclerView.setAdapter(movieLibraryAdapter);
    }
}
