package com.nicoberangerenico.moviesapp.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseNetworkException;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.miguelcatalan.materialsearchview.MaterialSearchView;
import com.nicoberangerenico.moviesapp.FirebaseManagement;
import com.nicoberangerenico.moviesapp.IFirebaseListener;
import com.nicoberangerenico.moviesapp.R;
import com.nicoberangerenico.moviesapp.adapter.GenreAdapter;
import com.nicoberangerenico.moviesapp.adapter.MovieHomeAdapter;
import com.nicoberangerenico.moviesapp.tools.ConstFile;
import com.nicoberangerenico.movieslib.API.IApiListner;
import com.nicoberangerenico.movieslib.API.MoviesProvider;
import com.nicoberangerenico.movieslib.Models.Enums.ExtraParam;
import com.nicoberangerenico.movieslib.Models.Genre;
import com.nicoberangerenico.movieslib.Models.Movie;
import com.nicoberangerenico.movieslib.Models.MoviesResult;
import com.nicoberangerenico.movieslib.SQLite.DatabaseAccess;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.search_view) MaterialSearchView searchView;

    @BindView(R.id.cardViewLibrary) CardView cardViewLibrary;
    @BindView(R.id.progressBarLibraryContainer) LinearLayout progressBarLibraryContainer;
    @BindView(R.id.recyclerViewLibrary) RecyclerView recyclerViewLibrary;
    @BindView(R.id.txt_no_result_library) TextView txtNoResultLibrary;

    @BindView(R.id.progressBarPopularContainer) LinearLayout progressBarPopularContainer;
    @BindView(R.id.recyclerViewPopularMovies) RecyclerView recyclerViewPopularMovies;
    @BindView(R.id.txt_no_result_popular_movies) TextView txtNoResultPopularMovies;

    @BindView(R.id.progressBarGenreContainer) LinearLayout progressBarGenreContainer;
    @BindView(R.id.recyclerViewGenre) RecyclerView recyclerViewGenre;
    @BindView(R.id.txt_no_result_genres) TextView txtNoResultGenres;

    @BindView(R.id.progressBarUpcomingContainer) LinearLayout progressBarUpcomingContainer;
    @BindView(R.id.recyclerViewUpcomingMovies) RecyclerView recyclerViewUpcomingMovies;
    @BindView(R.id.txt_no_result_upcoming_movies) TextView txtNoResultUpcomingMovies;

    @BindView(R.id.accountFullName) TextView txtAccountFullName;
    @BindView(R.id.accountEmail) TextView txtAccountEmail;
    @BindView(R.id.accountIcon) CircleImageView imgAccountIcon;

    @BindView(R.id.txtLogin) TextView txtLogin;
    @BindView(R.id.iconLogin) ImageView iconLogin;

    @BindView(R.id.swipeRefreshLayout) SwipeRefreshLayout swipeRefreshLayout;


    public static final int GOOGLE_SIGN_OUT = 2354;
    public static final int REFRESH_ON_RESULT = 5687;

    private FirebaseAuth mAuth;
    private boolean loggedIn;

    private DatabaseAccess databaseAccess;
    private MoviesProvider moviesProvider;

    private MovieHomeAdapter libraryMovieHomeAdapter;
    private List<Movie> libraryMovieList;

    private List<Movie> popularMovieList;
    private MovieHomeAdapter popularMovieHomeAdapter;

    private List<Genre> movieGenreList;
    private GenreAdapter genreAdapter;

    private List<Movie> upcomingMovieList;
    private MovieHomeAdapter upcomingMovieHomeAdapter;

    private static final int RC_SIGN_IN = 9001;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        databaseAccess = DatabaseAccess.getInstance(this);
        moviesProvider = MoviesProvider.getINSTANCE();

        mAuth = FirebaseAuth.getInstance();

        SharedPreferences sharedPreferences = getSharedPreferences(ConstFile.SHARED_PREF_LOGIN, Context.MODE_PRIVATE);
        loggedIn = sharedPreferences.getBoolean(ConstFile.SHARED_PREF_LOGIN, false);

        InitializeUI();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_menu, menu);

        MenuItem item = menu.findItem(R.id.action_search);
        searchView.setMenuItem(item);

        return true;
    }

    @Override
    public void onBackPressed() {
        if (searchView.isSearchOpen()) {
            searchView.closeSearch();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);

            try {
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                errorManager(e);
            }
        } else if (requestCode == GOOGLE_SIGN_OUT && resultCode == RESULT_OK) {
            loggedIn = false;

            InitializeInformations();
            refreshMyLibrary();
        } else {
            refreshMyLibrary();
        }
    }

    @OnClick(R.id.cardViewAccount)
    public void SettingsOnClick() {
        if (!loggedIn) {
            signInWithGoogle();
        } else {
            startActivityForResult(new Intent(this, SettingsActivity.class), GOOGLE_SIGN_OUT);
        }
    }

    @OnClick(R.id.btnMoreLibrary)
    public void MoreLibraryOnClick() {
        startActivityForResult(new Intent(this, LibraryActivity.class), REFRESH_ON_RESULT);
    }

    @OnClick(R.id.btnMorePopularMovies)
    public void MorePopularOnClick() {
        Intent intent = new Intent(this, MoviesActivity.class);
        intent.putExtra(MoviesActivity.TYPE_REQUEST_PARAM, ExtraParam.POPULAR.value());
        startActivityForResult(intent, REFRESH_ON_RESULT);
    }

    @OnClick(R.id.btnMoreGenres)
    public void MoreGenreOnClick() {
        startActivityForResult(new Intent(this, GenreActivity.class), REFRESH_ON_RESULT);
    }

    @OnClick(R.id.btnMoreUpcomingMovies)
    public void MoreUpcomingOnClick() {
        Intent intent = new Intent(this, MoviesActivity.class);
        intent.putExtra(MoviesActivity.TYPE_REQUEST_PARAM, ExtraParam.UP_COMING.value());
        startActivityForResult(intent, REFRESH_ON_RESULT);
    }

    /**
     * UI initialization
     */
    private void InitializeUI() {
        InitializeSwipeRefresh();
        InitializeSearchView();
        InitializeInformations();
        InitializeMyLibrary();
        InitializePopular();
        InitializeGenre();
        InitializeUpcoming();
    }

    private void InitializeSwipeRefresh() {
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshMyLibrary();
                refreshPopular();
                refreshGenre();
                refreshUpcoming();
                swipeRefreshLayout.setRefreshing(false);
            }
        });

        swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);
    }

    private void InitializeSearchView() {
        searchView.setVoiceSearch(false);
        searchView.setCursorDrawable(R.drawable.color_cursor_white);
        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Intent intent = new Intent(MainActivity.this, MoviesActivity.class);
                intent.putExtra(MoviesActivity.SEARCH_PARAM, query);
                startActivityForResult(intent, REFRESH_ON_RESULT);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
    }

    private void InitializeInformations() {
        if (loggedIn) {
            FirebaseUser currentUser = mAuth.getCurrentUser();

            if (currentUser != null) {
                String fullname = currentUser.getDisplayName();
                String email = currentUser.getEmail();

                txtAccountFullName.setText(fullname != null ? fullname : "");
                txtAccountEmail.setText(email != null ? email : "");

                if (currentUser.getPhotoUrl() != null) {
                    Picasso.get()
                            .load(currentUser.getPhotoUrl())
                            .noFade()
                            .into(imgAccountIcon);
                }
            }

            txtAccountFullName.setVisibility(View.VISIBLE);
            txtAccountEmail.setVisibility(View.VISIBLE);
            imgAccountIcon.setVisibility(View.VISIBLE);

            txtLogin.setVisibility(View.GONE);
            iconLogin.setVisibility(View.GONE);

        } else {
            txtAccountFullName.setVisibility(View.GONE);
            txtAccountEmail.setVisibility(View.GONE);
            imgAccountIcon.setVisibility(View.GONE);

            txtLogin.setVisibility(View.VISIBLE);
            iconLogin.setVisibility(View.VISIBLE);
        }
    }


    // // // // // INITIALIZE RECYCLER VIEW

    private void InitializeMyLibrary() {
        if (loggedIn) {
            cardViewLibrary.setVisibility(View.VISIBLE);
            recyclerViewLibrary.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
            recyclerViewLibrary.setItemAnimator(new DefaultItemAnimator());
            libraryMovieList = databaseAccess.getLimitedNumberMovies(15);

            progressBarLibraryContainer.setVisibility(View.GONE);

            if (libraryMovieList.size() == 0) {
                txtNoResultLibrary.setVisibility(View.VISIBLE);
                recyclerViewLibrary.setVisibility(View.GONE);
            }

            libraryMovieHomeAdapter = new MovieHomeAdapter(this, libraryMovieList);
            libraryMovieHomeAdapter.setListener(new MovieHomeAdapter.Listener() {
                @Override
                public void onMovieClick(Movie movie) {
                    Log.i(MovieDetailsActivity.MOVIE_PARAM, String.valueOf(movie));
                    Intent intent = new Intent(MainActivity.this, MovieDetailsActivity.class);
                    intent.putExtra(MovieDetailsActivity.MOVIE_PARAM, movie);
                    startActivityForResult(intent, REFRESH_ON_RESULT);
                }
            });
            recyclerViewLibrary.setAdapter(libraryMovieHomeAdapter);
        } else {
            cardViewLibrary.setVisibility(View.GONE);
        }
    }

    private void InitializePopular() {
        recyclerViewPopularMovies.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        recyclerViewPopularMovies.setItemAnimator(new DefaultItemAnimator());

        moviesProvider.getMoviesListPopular(1, new IApiListner<MoviesResult>() {
            @Override
            public void onSuccess(MoviesResult response) {
                progressBarPopularContainer.setVisibility(View.GONE);

                popularMovieList = response.moviesList;

                if (popularMovieList.size() == 0) {
                    txtNoResultPopularMovies.setVisibility(View.VISIBLE);
                    recyclerViewPopularMovies.setVisibility(View.GONE);
                }

                popularMovieHomeAdapter = new MovieHomeAdapter(MainActivity.this, popularMovieList);
                popularMovieHomeAdapter.setListener(new MovieHomeAdapter.Listener() {
                    @Override
                    public void onMovieClick(Movie movie) {
                        Log.i(MovieDetailsActivity.ID_MOVIE_PARAM, String.valueOf(movie.getId()));
                        Intent intent = new Intent(MainActivity.this, MovieDetailsActivity.class);
                        intent.putExtra(MovieDetailsActivity.ID_MOVIE_PARAM, movie.getId());
                        startActivityForResult(intent, REFRESH_ON_RESULT);
                    }
                });
                recyclerViewPopularMovies.setAdapter(popularMovieHomeAdapter);
            }

            @Override
            public void onError(String s) {
                progressBarPopularContainer.setVisibility(View.GONE);
                txtNoResultPopularMovies.setVisibility(View.VISIBLE);
                recyclerViewPopularMovies.setVisibility(View.GONE);

                Toast.makeText(MainActivity.this, R.string.get_popular_movies_on_error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void InitializeGenre() {
        recyclerViewGenre.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewGenre.setItemAnimator(new DefaultItemAnimator());

        movieGenreList = databaseAccess.getAllGenre().subList(0, 3);

        progressBarGenreContainer.setVisibility(View.GONE);

        genreAdapter = new GenreAdapter(this, movieGenreList, true);
        genreAdapter.setListener(new GenreAdapter.Listener() {
            @Override
            public void onGenreClick(Genre genre) {
                Log.i(MoviesActivity.CATEGORY_PARAM, String.valueOf(genre.getId()));
                Intent intent = new Intent(MainActivity.this, MoviesActivity.class);
                intent.putExtra(MoviesActivity.CATEGORY_PARAM, genre);
                startActivityForResult(intent, REFRESH_ON_RESULT);
            }
        });
        recyclerViewGenre.setAdapter(genreAdapter);
    }

    private void InitializeUpcoming() {
        recyclerViewUpcomingMovies.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        recyclerViewUpcomingMovies.setItemAnimator(new DefaultItemAnimator());

        moviesProvider.getMoviesListUpComing(1, new IApiListner<MoviesResult>() {
            @Override
            public void onSuccess(MoviesResult response) {
                progressBarUpcomingContainer.setVisibility(View.GONE);

                upcomingMovieList = response.moviesList;

                if (upcomingMovieList.size() == 0) {
                    txtNoResultUpcomingMovies.setVisibility(View.VISIBLE);
                    recyclerViewUpcomingMovies.setVisibility(View.GONE);
                }

                upcomingMovieHomeAdapter = new MovieHomeAdapter(MainActivity.this, upcomingMovieList);
                upcomingMovieHomeAdapter.setListener(new MovieHomeAdapter.Listener() {
                    @Override
                    public void onMovieClick(Movie movie) {
                        Log.i(MovieDetailsActivity.ID_MOVIE_PARAM, String.valueOf(movie.getId()));
                        Intent intent = new Intent(MainActivity.this, MovieDetailsActivity.class);
                        intent.putExtra(MovieDetailsActivity.ID_MOVIE_PARAM, movie.getId());
                        startActivityForResult(intent, REFRESH_ON_RESULT);
                    }
                });
                recyclerViewUpcomingMovies.setAdapter(upcomingMovieHomeAdapter);
            }

            @Override
            public void onError(String s) {
                progressBarUpcomingContainer.setVisibility(View.GONE);
                txtNoResultUpcomingMovies.setVisibility(View.VISIBLE);
                recyclerViewUpcomingMovies.setVisibility(View.GONE);

                Toast.makeText(MainActivity.this, R.string.get_upcoming_movies_on_error, Toast.LENGTH_SHORT).show();
            }
        });
    }


    // // // // // REFRESH RECYCLER VIEW

    private void refreshMyLibrary() {
        if (loggedIn) {
            cardViewLibrary.setVisibility(View.VISIBLE);

            if (libraryMovieList == null) {
                InitializeMyLibrary();
            } else {
                libraryMovieList.clear();
                libraryMovieList.addAll(databaseAccess.getLimitedNumberMovies(15));

                if (libraryMovieList.size() == 0) {
                    txtNoResultLibrary.setVisibility(View.VISIBLE);
                    recyclerViewLibrary.setVisibility(View.GONE);
                }

                libraryMovieHomeAdapter.notifyDataSetChanged();
            }
        } else {
            cardViewLibrary.setVisibility(View.GONE);
        }
    }

    private void refreshPopular() {
        moviesProvider.getMoviesListPopular(1, new IApiListner<MoviesResult>() {
            @Override
            public void onSuccess(MoviesResult response) {
                progressBarPopularContainer.setVisibility(View.GONE);

                if (popularMovieList == null) {
                    InitializePopular();
                } else {
                    popularMovieList.clear();
                    popularMovieList.addAll(response.moviesList);

                    if (popularMovieList.size() == 0) {
                        txtNoResultPopularMovies.setVisibility(View.VISIBLE);
                        recyclerViewPopularMovies.setVisibility(View.GONE);
                    }

                    popularMovieHomeAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onError(String s) {
                Toast.makeText(MainActivity.this, R.string.get_popular_movies_on_error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void refreshGenre() {
        if (movieGenreList == null) {
            InitializeGenre();
        } else {
            movieGenreList.clear();
            movieGenreList.addAll(databaseAccess.getAllGenre().subList(0, 3));
            progressBarGenreContainer.setVisibility(View.GONE);
            genreAdapter.notifyDataSetChanged();
        }
    }

    private void refreshUpcoming() {
        moviesProvider.getMoviesListUpComing(1, new IApiListner<MoviesResult>() {
            @Override
            public void onSuccess(MoviesResult response) {
                progressBarUpcomingContainer.setVisibility(View.GONE);

                if (upcomingMovieList == null) {
                    InitializeUpcoming();
                } else {
                    upcomingMovieList.clear();
                    upcomingMovieList.addAll(response.moviesList);

                    if (upcomingMovieList.size() == 0) {
                        txtNoResultUpcomingMovies.setVisibility(View.VISIBLE);
                        recyclerViewUpcomingMovies.setVisibility(View.GONE);
                    }

                    upcomingMovieHomeAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onError(String s) {
                Toast.makeText(MainActivity.this, R.string.get_upcoming_movies_on_error, Toast.LENGTH_SHORT).show();
            }
        });
    }


    // // // // // GOOGLE SIGN IN

    private void signInWithGoogle() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        startActivityForResult(GoogleSignIn.getClient(this, gso).getSignInIntent(), RC_SIGN_IN);
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount googleSignInAccount) {

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(getString(R.string.signing_in));
        progressDialog.show();

        AuthCredential credential = GoogleAuthProvider.getCredential(googleSignInAccount.getIdToken(), null);
        mAuth.signInWithCredential(credential).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful())
                    onSignInResult();
                else
                    errorManager(task);

                try {
                    progressDialog.dismiss();
                } catch (Exception ignored) {
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                errorManager(e);

                try {
                    progressDialog.dismiss();
                } catch (Exception ignored) {
                }
            }
        });
    }

    private void onSignInResult() {
        SharedPreferences.Editor editor = getSharedPreferences(ConstFile.SHARED_PREF_LOGIN, Context.MODE_PRIVATE).edit();
        editor.putBoolean(ConstFile.SHARED_PREF_LOGIN, true).apply();

        loggedIn = true;

        cardViewLibrary.setVisibility(View.VISIBLE);

        FirebaseManagement.downloadDatabase(this, mAuth.getUid(), new IFirebaseListener() {
            @Override
            public void onDataChange() {
                refreshMyLibrary();
            }
        });

        InitializeInformations();
    }

    private void errorManager(Task<AuthResult> task) {
        errorManager(task.getException());
    }

    private void errorManager(Exception exception) {
        try {
            throw exception;
        } catch (FirebaseNetworkException e) {
            Toast.makeText(this, R.string.error_network, Toast.LENGTH_SHORT).show();
        } catch (ApiException e) {
            Toast.makeText(this, R.string.somthings_went_wrong, Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Toast.makeText(this, e.getClass().getSimpleName() + ": " + e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
        }
    }
}
