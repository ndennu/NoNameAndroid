package com.nicoberangerenico.moviesapp.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.constraint.Group;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.view.MenuItem;
import android.widget.Toolbar;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.zagum.switchicon.SwitchIconView;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.nicoberangerenico.moviesapp.R;
import com.nicoberangerenico.moviesapp.adapter.MovieDetailsAdapter;
import com.nicoberangerenico.moviesapp.adapter.MovieDetailsVideosAdapter;
import com.nicoberangerenico.moviesapp.tools.ConstFile;
import com.nicoberangerenico.movieslib.API.IApiListner;
import com.nicoberangerenico.movieslib.API.MoviesProvider;
import com.nicoberangerenico.moviesapp.ui.LinePagerIndicatorDecoration;
import com.nicoberangerenico.movieslib.Models.Genre;
import com.nicoberangerenico.movieslib.Models.LanguagesMovie;
import com.nicoberangerenico.movieslib.Models.Movie;
import com.nicoberangerenico.movieslib.Models.MovieVideos;
import com.nicoberangerenico.movieslib.Models.MoviesResult;
import com.nicoberangerenico.movieslib.Models.ProductionCompanies;
import com.nicoberangerenico.movieslib.SQLite.DatabaseAccess;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MovieDetailsActivity extends YouTubeBaseActivity implements View.OnClickListener {

    @BindView(R.id.movie_cover) ImageView movie_cover;
    @BindView(R.id.movie_name) TextView movie_name;
    @BindView(R.id.movie_release_date_details) TextView movie_release_date_details;
    @BindView(R.id.movie_overview) TextView movie_overview;
    @BindView(R.id.expend_text) TextView expend_text;
    @BindView(R.id.movie_production_details) TextView movie_production_details;

    @BindView(R.id.movie_gender_details) TextView movie_gender_details;
    @BindView(R.id.movie_runtime) TextView movie_runtime;
    @BindView(R.id.movie_original_language) TextView movie_orignal_language;

    @BindView(R.id.notation_txt) TextView notation_txt;
    @BindView(R.id.ratingBar) RatingBar ratingBar;

    @BindView(R.id.card_view_like) CardView card_view_like;
    @BindView(R.id.movie_tv_like) TextView movie_tv_like;
    @BindView(R.id.movie_image_like) SwitchIconView movie_image_like;

    @BindView(R.id.video_group) Group video_group;
    @BindView(R.id.movie_video_recycler_view) RecyclerView movie_video_recycler_view;
    @BindView(R.id.txt_no_result_videos_details) TextView txt_no_result_videos_details;

    @BindView(R.id.card_view_similary) CardView card_view_similary;
    @BindView(R.id.movie_similar_recycler_view) RecyclerView movie_similar_recycler_view;
    @BindView(R.id.txt_no_result_details) TextView txt_no_result_details;

    @BindView(R.id.toolbar) Toolbar toolbar;

    private long movie_id;
    private Movie movie;
    public static final String ID_MOVIE_PARAM = "ID_MOVIE_PARAM";
    public static final String MOVIE_PARAM = "MOVIE_PARAM";
    private boolean loggedIn;

    private boolean isCheck;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.movie_detail);

        ButterKnife.bind(this);

        setActionBar(toolbar);
        Objects.requireNonNull(getActionBar()).setDisplayHomeAsUpEnabled(true);

        SharedPreferences sharedPreferences = getSharedPreferences(ConstFile.SHARED_PREF_LOGIN, Context.MODE_PRIVATE);
        loggedIn = sharedPreferences.getBoolean(ConstFile.SHARED_PREF_LOGIN, false);

        initializeData();
        initializeLayout();

        PagerSnapHelper snapHelper = new PagerSnapHelper();
        snapHelper.attachToRecyclerView(movie_video_recycler_view);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Initialisation of MoviesProvider and get movie id or movie object from intent
     */
    private void initializeData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle == null) return;

        isCheck = true;

        MoviesProvider swProvider = MoviesProvider.getINSTANCE();

        if (getIntent().getLongExtra(ID_MOVIE_PARAM, 0) != 0) {
            movie_id = getIntent().getLongExtra(ID_MOVIE_PARAM, 0);
            getMovieDetails(swProvider);
            getSimilarMovies(swProvider);
            getMovieVideos(swProvider);
        } else {
            movie = (Movie)getIntent().getSerializableExtra(MOVIE_PARAM);
            movie_id = movie.getId();
            updateView(movie);
            getMovieDetails(swProvider);
            getSimilarMovies(swProvider);
            getMovieVideos(swProvider);
        }

        if (movie != null) {
            Log.i("IDMovie_DETAILS_OBJ", String.valueOf(movie.getId()));
        }
        if (movie_id == 0) {
            //TODO: ERREUR
            return;
        }

        onLike();
    }

    /**
     * Initialisation of recycler views layout
     */
    private void initializeLayout() {
        movie_similar_recycler_view.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        movie_video_recycler_view.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
    }

    /**
     * Call getMovieDetails to get movie informations from id
     * @param swProvider
     */
    public void getMovieDetails(final MoviesProvider swProvider) {
        swProvider.getMovieDetails(movie_id, new IApiListner<Movie>() {
            @Override
            public void onSuccess(Movie response) {
                updateView(response);
            }
            @Override
            public void onError(String s) {
                Toast.makeText(MovieDetailsActivity.this,"Erreur lors du chargement des données.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * Call getSimilarMovies to get similar movie of current movie from id
     * @param swProvider
     */
    public void getSimilarMovies(MoviesProvider swProvider) {
        swProvider.getSimilarMovies(movie_id, new IApiListner<MoviesResult>() {
            @Override
            public void onSuccess(MoviesResult response) {
                updateViewSimilar(response);
            }

            @Override
            public void onError(String s) {
                txt_no_result_details.setVisibility(View.VISIBLE);
                movie_similar_recycler_view.setVisibility(View.GONE);

                Toast.makeText(MovieDetailsActivity.this,"Erreur lors du chargement des films similaires.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * Call getMovieVideos to get all videos linked to the current movie from id
     * @param swProvider
     */
    public void getMovieVideos(MoviesProvider swProvider) {
        swProvider.getMovieVideos(movie_id, new IApiListner<MovieVideos>() {
            @Override
            public void onSuccess(MovieVideos response) {
                updateViewVideo(response);
            }

            @Override
            public void onError(String s) {
                txt_no_result_videos_details.setVisibility(View.VISIBLE);
                movie_video_recycler_view.setVisibility(View.GONE);

                Toast.makeText(MovieDetailsActivity.this,"Erreur lors du chargement des vidéos.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * Update similar movies recycler view with received date from api
     * @param moviesResult
     */
    public void updateViewSimilar(MoviesResult moviesResult) {
        MovieDetailsAdapter moviesAdapter = new MovieDetailsAdapter(moviesResult.getMoviesList());
        moviesAdapter.setListener(new MovieDetailsAdapter.Listener() {
            @Override
            public void onMovieClick(Movie movie) {
                Log.i(MovieDetailsActivity.ID_MOVIE_PARAM, String.valueOf(movie.getId()));
                Intent intent = new Intent(MovieDetailsActivity.this, MovieDetailsActivity.class);
                intent.putExtra(MovieDetailsActivity.ID_MOVIE_PARAM, movie.getId());
                intent.putExtra(MovieDetailsActivity.MOVIE_PARAM, movie);
                startActivity(intent);
            }
        });
        movie_similar_recycler_view.setAdapter(moviesAdapter);
    }

    /**
     * Update videos recycler view with received data from api
     * @param movieVideos
     */
    public void updateViewVideo(MovieVideos movieVideos) {

        if (movieVideos.getVideos_list().size() == 0) {
            video_group.setVisibility(View.GONE);
        }

        MovieDetailsVideosAdapter movieDetailsVideosAdapter = new MovieDetailsVideosAdapter(movieVideos.getVideos_list(), this);
        movie_video_recycler_view.setAdapter(movieDetailsVideosAdapter);
        movie_video_recycler_view.addItemDecoration(new LinePagerIndicatorDecoration());
    }

    /**
     * Update view when she's created to initialize all the informations (text view, videos, images, etc.)
     * @param movieDetails
     */
    public void updateView(Movie movieDetails) {

        if(movieDetails.getPosterPath() != null && movieDetails.getPosterPath().compareTo("") != 0)
            Picasso.get()
                    .load("https://image.tmdb.org/t/p/" + "w185" + movieDetails.getPosterPath())
                    .fit()
                    .into(movie_cover);

        movie_name.setText(movieDetails.getTitle());

        movie_release_date_details.setText(String.valueOf(formattingDate(movieDetails.getReleaseDate())));

        movie_overview.setText(movieDetails.getOverview());

        movie_overview.setOnClickListener(this);
        expend_text.setOnClickListener(this);

        StringBuilder production = setString(movieDetails.getProductionCompanies(), "production");
        movie_production_details.setText(production);

        DecimalFormat format = new DecimalFormat("0.0");
        ratingBar.setRating(movieDetails.getVoteAverage()/2);

        notation_txt.setText(format.format(movieDetails.getVoteAverage()/2));

        StringBuilder genders = setString(movieDetails.getGenresList(), "genders");
        movie_gender_details.setText(genders);

        movie_runtime.setText(String.valueOf(String.format("%s mn", Integer.toString(movieDetails.getRuntime()))));

        StringBuilder languages;
        languages = setString(movieDetails.getSpokenLanguages(), "languages");
        if (languages.length() == 0) { languages.append("English"); }

        movie_orignal_language.setText(languages);
    }

    @Override
    public void onClick(View v) {
        if (isCheck) {
            expend_text.setText("Voir moins...");
            movie_overview.setMaxLines(20);
            isCheck = false;
        } else {
            expend_text.setText("Voir plus...");
            movie_overview.setMaxLines(5);
            isCheck = true;
        }
    }

    /**
     * Allow set a format to the string parameter with data
     * @param list
     * @param type
     * @return String Builder
     */
    private StringBuilder setString(List list, String type) {

        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0; i < list.size(); i++) {
            if (type == "genders") {
                stringBuilder.append((( Genre ) list.get(i)).getName());
            } else if (type == "languages") {
                stringBuilder.append((( LanguagesMovie ) list.get(i)).getName());
            } else if (type == "production") {
                stringBuilder.append((( ProductionCompanies ) list.get(i)).getName());
            }

            if (i != list.size() - 1) {
                stringBuilder.append(", ");
            }
        }

        return stringBuilder;
    }

    /**
     * Allow to set a format date
     * @param date
     * @return
     */
    private String formattingDate(String date) {

        SimpleDateFormat oldFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        try {
            String newDate = dateFormat.format(oldFormat.parse(date));
            Log.i("Date", newDate.toString());
            return newDate;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Allow to manage the like button
     */
    public void onLike() {

        final DatabaseAccess access = DatabaseAccess.getInstance(MovieDetailsActivity.this);
        if (access.movieExist(movie_id)) {
            movie_image_like.setIconEnabled(true);
        }


            movie_image_like.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (loggedIn) {
                        if (access.movieExist(movie_id)) {
                            access.deleteMovie(movie_id);
                            movie_image_like.switchState();
                        }
                        else {
                            MoviesProvider.getINSTANCE().getMovieDetails(movie_id, new IApiListner<Movie>() {
                                @Override
                                public void onSuccess(Movie response) {
                                    access.insertMovie(response);
                                    movie_image_like.switchState();
                                }

                                @Override
                                public void onError(String s) {
                                    Toast.makeText(MovieDetailsActivity.this, "Error lors de la sauvegarde", Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    } else {
                        Toast.makeText(MovieDetailsActivity.this, "Vous devez vous connecter !", Toast.LENGTH_SHORT).show();
                    }
                }
            });

    }

}
