package com.nicoberangerenico.moviesapp.activity;

import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.miguelcatalan.materialsearchview.MaterialSearchView;
import com.nicoberangerenico.moviesapp.R;
import com.nicoberangerenico.moviesapp.adapter.MoviesAdapter;
import com.nicoberangerenico.moviesapp.ui.RecyclerOnScrollListener;
import com.nicoberangerenico.movieslib.API.IApiListner;
import com.nicoberangerenico.movieslib.API.MoviesProvider;
import com.nicoberangerenico.movieslib.Models.Enums.ExtraParam;
import com.nicoberangerenico.movieslib.Models.Genre;
import com.nicoberangerenico.movieslib.Models.Movie;
import com.nicoberangerenico.movieslib.Models.MoviesResult;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MoviesActivity extends AppCompatActivity {

    public static final String TYPE_REQUEST_PARAM = "TYPE_REQUEST_PARAM";
    public static final String SEARCH_PARAM = "SEARCH_PARAM";
    public static final String CATEGORY_PARAM = "CATEGORY_PARAM";

    // [INFO] - variable sorted by Asc name
    @BindView(R.id.search_view) MaterialSearchView searchView;
    @BindView(R.id.movies_recycler_view) RecyclerView moviesRecyclerView;
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.swipeContainer) SwipeRefreshLayout swipeRefreshLayout;

    private MoviesAdapter moviesAdapter;
    private List<Movie> moviesList;

    private int currentPage;
    private ApiRequestType requestType;

    private String searchParam;
    private Genre genderSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movies);

        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        initRecyclerView();
        InitializeToolBar();
        InitializePullToRefresh();
        searchParam =  getIntent().getStringExtra(SEARCH_PARAM);
        genderSearch = (Genre)getIntent().getSerializableExtra(CATEGORY_PARAM);

        ExtraParam wsType = ExtraParam.values()[getIntent().getIntExtra(TYPE_REQUEST_PARAM, 0)];

        currentPage = 1;

        Log.i(TYPE_REQUEST_PARAM, String.valueOf(wsType.toString()));
        Log.i(SEARCH_PARAM, searchParam + " ");

        if (searchParam != null) {
            getMoviesListSearch(MoviesProvider.getINSTANCE(), searchParam, 1);
            setTitle(searchParam);
            return;
        }

        if (genderSearch != null) {
            getMovieWithGenderListSearch(MoviesProvider.getINSTANCE(), genderSearch.getId(), 1);
            setTitle(String.valueOf(genderSearch.getName()));
            return;
        }

        // TODO: MAKE A LOADER
        initData(wsType);
    }

    /* ********************** *
        TOOL BAR SECTION
     * ********************** */

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_menu, menu);

        MenuItem item = menu.findItem(R.id.action_search);
        searchView.setMenuItem(item);
        return true;
    }

    @Override
    public void onBackPressed() {
        if (searchView.isSearchOpen()) {
            searchView.closeSearch();
        } else {
            super.onBackPressed();
        }
    }

    private void InitializeToolBar() {
        //SearchView Initialization
        searchView.setVoiceSearch(false);
        searchView.setCursorDrawable(R.drawable.color_cursor_white);
        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                MoviesProvider moviesProvider = MoviesProvider.getINSTANCE();
                getMoviesListSearch(moviesProvider, query, 1);

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
    }

    private void initRecyclerView() {
        moviesList = new ArrayList<>();
        moviesRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        moviesAdapter = new MoviesAdapter(this, moviesList);
        moviesAdapter.setListener(new MoviesAdapter.Listener() {
            @Override
            public void onMovieClick(Movie movie) {
                Log.i(MovieDetailsActivity.ID_MOVIE_PARAM, String.valueOf(movie.getId()));
                Intent intent = new Intent(MoviesActivity.this, MovieDetailsActivity.class);
                intent.putExtra(MovieDetailsActivity.ID_MOVIE_PARAM, movie.getId());
                startActivity(intent);
            }
        });
        moviesRecyclerView.setAdapter(moviesAdapter);
        Log.i("MOVIES_LIST_SUCC",  "LIST LOADED");

        moviesRecyclerView.addOnScrollListener(new RecyclerOnScrollListener((LinearLayoutManager) moviesRecyclerView.getLayoutManager()) {
            @Override
            public void onLoadMore(int next_page) {
                loadMoreMovies(next_page);
            }
        });
    }

    private void InitializePullToRefresh() {
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                ExtraParam wsType = ExtraParam.values()[getIntent().getIntExtra(TYPE_REQUEST_PARAM, 0)];
                initData(wsType);
            }
        });
        swipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

    }

    /* ********************** *
        DATA SECTION
     * ********************** */

    private void initData(ExtraParam wsType) {
        MoviesProvider moviesProvider = MoviesProvider.getINSTANCE();
        switch (wsType) {
            case POPULAR:
                getMoviesPopular(moviesProvider, 1);
                setTitle(getString(R.string.popular_movies));
                break;
            case UP_COMING:
                getMoviesUpComming(moviesProvider, 1);
                setTitle(getString(R.string.upcoming_movies));
                break;
        }
    }

    private void getMoviesPopular(MoviesProvider moviesProvider, int page) {
        if(page == 1) {
            requestType = ApiRequestType.MoviesPopular;
            currentPage = 1;
        }

        moviesProvider.getMoviesListPopular(page, new IApiListner<MoviesResult>() {
            @Override
            public void onSuccess(MoviesResult response) {
                getMoviesSuccess(response);
            }

            @Override
            public void onError(String s) {
                getMoviesFailure();
            }
        });
    }

    private void getMoviesUpComming(MoviesProvider moviesProvider, int page) {
        if(page == 1) {
            requestType = ApiRequestType.MoviesUpComing;
            currentPage = 1;
        }

        moviesProvider.getMoviesListUpComing(page, new IApiListner<MoviesResult>() {
            @Override
            public void onSuccess(MoviesResult response) {
                getMoviesSuccess(response);
            }

            @Override
            public void onError(String s) {
                getMoviesFailure();
            }
        });
    }

    private void getMoviesListSearch(MoviesProvider moviesProvider, String search, int page) {
        if(page == 1) {
            requestType = ApiRequestType.MoviesListSearch;
            currentPage = 1;
        }

        Log.i("(GET) SEARCH_MOVIES",  String.format("SEARCH_VALUE: %s", search));

        moviesProvider.getMoviesListSearch(search, page, new IApiListner<MoviesResult>() {
            @Override
            public void onSuccess(MoviesResult response) {
                getMoviesSuccess(response);
            }

            @Override
            public void onError(String s) {
                getMoviesFailure();
            }
        });
    }


    private void getMovieWithGenderListSearch(MoviesProvider moviesProvider, int category, int page) {
        if(page == 1) {
            requestType = ApiRequestType.MovieWithGenderListSearch;
            currentPage = 1;
        }

        Log.i("(GET) SEARCH_MOVIES",  String.format("SEARCH_VALUE: %d", category));

        moviesProvider.getMoviesWithGender(category, page, new IApiListner<MoviesResult>() {
            @Override
            public void onSuccess(MoviesResult response) {
                getMoviesSuccess(response);
            }

            @Override
            public void onError(String s) {
                getMoviesFailure();
            }
        });
    }

    /* ********************** *
        MANAGE SUCCESS OR ERROR WS
     * ********************** */

    private void getMoviesSuccess(MoviesResult moviesResult) {
        if(currentPage == 1){
            moviesList = moviesResult.getMoviesList();
        } else {
            moviesList.addAll(moviesResult.getMoviesList());
        }

        moviesAdapter.setMoviesList(moviesList);
        moviesAdapter.notifyDataSetChanged();
        swipeRefreshLayout.setRefreshing(false);
    }

    private void loadMoreMovies(int page) {
        currentPage = page;

        MoviesProvider moviesProvider = MoviesProvider.getINSTANCE();

        switch (requestType) {
            case MoviesPopular:
                getMoviesPopular(moviesProvider, page);
                break;
            case MoviesUpComing:
                getMoviesUpComming(moviesProvider, page);
                break;
            case MoviesListSearch:
                getMoviesListSearch(moviesProvider, searchParam, page);
                break;
            case MovieWithGenderListSearch:
                getMovieWithGenderListSearch(moviesProvider, genderSearch.getId(), page);
                break;
        }
    }

    private void getMoviesFailure() {
        // TODO: USER FRIENDLY ERROR
    }

    private enum ApiRequestType {
        MoviesPopular(0),
        MoviesUpComing(1),
        MoviesListSearch(2),
        MovieWithGenderListSearch(3);

        private int param;

        ApiRequestType(int param) {
            this.param = param;
        }

        public int value() {
            return param;
        }
    }
}
