package com.nicoberangerenico.moviesapp.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.nicoberangerenico.moviesapp.R;

public class SettingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        setTitle(R.string.settings);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getSupportFragmentManager().beginTransaction().replace(R.id.preferences_content, new SettingsFragment()).commit();
    }
}
