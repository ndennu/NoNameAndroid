package com.nicoberangerenico.moviesapp.activity;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.nicoberangerenico.moviesapp.FirebaseManagement;
import com.nicoberangerenico.moviesapp.IFirebaseListener;
import com.nicoberangerenico.moviesapp.R;
import com.nicoberangerenico.moviesapp.tools.ConstFile;
import com.nicoberangerenico.movieslib.SQLite.DatabaseAccess;

import java.util.Objects;

public class SettingsFragment extends PreferenceFragmentCompat {
    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        addPreferencesFromResource(R.xml.settings_preferences_screen);

        initUI(getContext());
    }


    private void initUI(final Context context) {
        findPreference("clean_data").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                new AlertDialog.Builder(context)
                        .setTitle(R.string.clean_data_tilte)
                        .setMessage(R.string.clean_data_summary)
                        .setPositiveButton(R.string.clean, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                DatabaseAccess.getInstance(context).cleanDatabase();
                                Snackbar.make(getListView(), R.string.clean_data_cleaned, Snackbar.LENGTH_LONG).show();
                            }
                        })
                        .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {}
                        })
                        .show();
                return false;
            }
        });

        findPreference("upload_data").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                new AlertDialog.Builder(context)
                        .setTitle(R.string.upload_data_title)
                        .setMessage(R.string.upload_data_summary)
                        .setPositiveButton(R.string.save, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                FirebaseManagement.uploadDatabase(context, FirebaseAuth.getInstance().getUid(), new IFirebaseListener() {
                                    @Override
                                    public void onDataChange() {
                                        Toast.makeText(context, R.string.firebase_upload_success, Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }
                        }).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {}
                        })
                        .show();
                return false;
            }
        });

        findPreference("download_data").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                new AlertDialog.Builder(context)
                        .setTitle(R.string.download_data_title)
                        .setMessage(R.string.download_data_summary)
                        .setPositiveButton(R.string.sync, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                FirebaseManagement.downloadDatabase(context, FirebaseAuth.getInstance().getUid(), new IFirebaseListener() {
                                    @Override
                                    public void onDataChange() {}
                                });
                            }
                        })
                        .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {}
                        })
                        .show();
                return false;
            }
        });

        findPreference("sign_out").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                new AlertDialog.Builder(context)
                        .setTitle(R.string.sign_out_title)
                        .setMessage(R.string.sign_out_message)
                        .setPositiveButton(R.string.sign_out, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                FirebaseAuth.getInstance().signOut();
                                DatabaseAccess.getInstance(context).cleanDatabase();

                                SharedPreferences.Editor editor = context.getSharedPreferences(ConstFile.SHARED_PREF_LOGIN, Context.MODE_PRIVATE).edit();
                                editor.putBoolean(ConstFile.SHARED_PREF_LOGIN, false).apply();
                                editor.apply();

                                Toast.makeText(context, R.string.signed_out, Toast.LENGTH_SHORT).show();

                                Objects.requireNonNull(getActivity()).setResult(Activity.RESULT_OK);
                                getActivity().finish();
                            }
                        })
                        .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {}
                        })
                        .show();
                return false;
            }
        });
    }
}
