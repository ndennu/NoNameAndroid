package com.nicoberangerenico.moviesapp.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import com.nicoberangerenico.movieslib.API.IApiListner;
import com.nicoberangerenico.movieslib.API.MoviesProvider;
import com.nicoberangerenico.movieslib.Models.Genre;
import com.nicoberangerenico.movieslib.Models.GenreResult;
import com.nicoberangerenico.movieslib.SQLite.DatabaseAccess;

import java.util.Iterator;
import java.util.List;

public class SplashScreenActivity extends AppCompatActivity {

    private DatabaseAccess databaseAccess;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        databaseAccess = DatabaseAccess.getInstance(this);

        MoviesProvider moviesProvider = MoviesProvider.getINSTANCE();
        moviesProvider.setContext(this);

        getGenreFromApi(moviesProvider);
    }

    private void getGenreFromApi(final MoviesProvider moviesProvider) {
        moviesProvider.getGenres(new IApiListner<GenreResult>() {
            @Override
            public void onSuccess(GenreResult response) {
                List<Genre> genreList = response.getGenreList();

                for (Iterator<Genre> iterator = response.iterator(); iterator.hasNext();) {
                    databaseAccess.insertGenreIfNotExist(iterator.next());
                }

                /*for (int i = 0; i < genreList.size(); i++){
                    databaseAccess.insertGenreIfNotExist(genreList.get(i));
                }*/

                changeActivity();
            }

            @Override
            public void onError(String s) {
                getCategoryFailure(moviesProvider);
            }
        });
    }

    private void getCategoryFailure(final MoviesProvider moviesProvider) {
        if(databaseAccess.countGenre() > 0){
            changeActivity();
        } else {
            new AlertDialog.Builder(SplashScreenActivity.this)
                    .setTitle("Error")
                    .setMessage("Network Error")
                    .setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            getGenreFromApi(moviesProvider);
                        }
                    })
                    .show();
        }
    }

    private void changeActivity(){
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }
}
