package com.nicoberangerenico.moviesapp.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nicoberangerenico.moviesapp.R;
import com.nicoberangerenico.movieslib.Models.Genre;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GenreAdapter extends RecyclerView.Adapter<GenreAdapter.ViewHolder> {

    private List<Genre> genreList;
    private boolean darkMode;
    private Context context;
    private Listener listener;

    public GenreAdapter(Context context, List<Genre> genreList) {
        this(context, genreList, false);
    }

    public GenreAdapter(Context context, List<Genre> genreList, boolean darkMode) {
        this.genreList = genreList;
        this.darkMode = darkMode;
        this.context = context;
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.item_genre, parent, false);
        return new GenreAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Genre genre = genreList.get(position);

        holder.iconGenre.setImageResource(getIconByGenreId(genre.getId()));
        holder.txtGenre.setText(genre.getName());

        if(darkMode){
            holder.constraintLayoutGenre.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
            holder.txtGenre.setTextColor(context.getResources().getColor(R.color.white));
            holder.iconGenre.setColorFilter(context.getResources().getColor(R.color.white));


        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) listener.onGenreClick(genre);
            }
        });
    }

    @Override
    public int getItemCount() {
        return genreList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.constraintLayoutGenre) ConstraintLayout constraintLayoutGenre;
        @BindView(R.id.icon_genre) ImageView iconGenre;
        @BindView(R.id.txt_genre) TextView txtGenre;

        private ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


    private int getIconByGenreId(int genreId){
        switch (genreId){
            case 12: return R.drawable.compass;
            case 14: return R.drawable.unicorn;
            case 16: return R.drawable.disney;
            case 18: return R.drawable.drama;
            case 27: return R.drawable.horror;
            case 28: return R.drawable.gun;
            case 35: return R.drawable.smile;
            case 36: return R.drawable.book;
            case 37: return R.drawable.cactus;
            case 53:
            case 80: return R.drawable.knife;
            case 99: return R.drawable.video;
            case 878: return R.drawable.rocket;
            case 9648: return R.drawable.question;
            case 10402: return R.drawable.music;
            case 10749: return R.drawable.heart;
            case 10751: return R.drawable.family;
            case 10752: return R.drawable.tank;
            case 10770: return R.drawable.tv;
            default: return R.drawable.film;
        }
    }

    public interface Listener {
        void onGenreClick(Genre genre);
    }
}
