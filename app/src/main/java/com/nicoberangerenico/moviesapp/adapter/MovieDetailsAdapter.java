package com.nicoberangerenico.moviesapp.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nicoberangerenico.moviesapp.R;
import com.nicoberangerenico.movieslib.Models.Movie;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MovieDetailsAdapter extends RecyclerView.Adapter<MovieDetailsAdapter.MovieSimilarHolder> {

    private List<Movie> similarMoviesList;
    private Listener listener;

    public MovieDetailsAdapter(List<Movie> similarMoviesList) {
        this.similarMoviesList = similarMoviesList;
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public MovieSimilarHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.movie_similary_item, parent, false);
        return new MovieSimilarHolder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull MovieSimilarHolder holder, int position) {

        final Movie movie = similarMoviesList.get(position);

        if(movie.getPosterPath() != null && movie.getPosterPath().compareTo("") != 0)
            Picasso.get()
                    .load("https://image.tmdb.org/t/p/" + "w185" + movie.getPosterPath())
                    .fit()
                    .into(holder.movie_similary_cover);

        holder.movie_similary_name.setText(movie.getTitle());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) listener.onMovieClick(movie);
            }
        });
    }

    @Override
    public int getItemCount() {
        return similarMoviesList == null ? 0 : similarMoviesList.size();
    }


    public class MovieSimilarHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.movie_similary_cover) ImageView movie_similary_cover;
        @BindView(R.id.movie_similary_name) TextView movie_similary_name;

        public MovieSimilarHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }
    }

    public interface Listener {
        void onMovieClick(Movie movie);
    }
}
