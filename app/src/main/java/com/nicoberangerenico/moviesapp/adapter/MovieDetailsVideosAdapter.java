package com.nicoberangerenico.moviesapp.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.nicoberangerenico.moviesapp.R;
import com.nicoberangerenico.movieslib.Models.Movie;
import com.nicoberangerenico.movieslib.Models.Videos;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MovieDetailsVideosAdapter extends RecyclerView.Adapter<MovieDetailsVideosAdapter.MovieVideoHolder>  {

    private List<Videos> videosList;
    private Context context;
    private String key;

    public MovieDetailsVideosAdapter(List<Videos> videosList, Context context) {
        this.videosList = videosList;
        this.context = context;
    }

    @NonNull
    @Override
    public MovieDetailsVideosAdapter.MovieVideoHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.movie_video_item, parent, false);
        return new MovieDetailsVideosAdapter.MovieVideoHolder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull MovieDetailsVideosAdapter.MovieVideoHolder holder, int position) {}

    @Override
    public void onViewAttachedToWindow(@NonNull MovieVideoHolder holder) {
        super.onViewAttachedToWindow(holder);
        int position = holder.getAdapterPosition();
        Videos movie = videosList.get(position);
        final String movieKey = movie.getKey();
        holder.movie_video_view.initialize(context.getString(R.string.youtube_api_key), new YouTubePlayer.OnInitializedListener() {
            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
                if(null== youTubePlayer) {
                    Toast.makeText(context, "Erreur lors du chargement des videos", Toast.LENGTH_LONG).show();
                    return;
                }

                if (!b) {
                    youTubePlayer.cueVideo(movieKey);
                }
            }

            @Override
            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
                Toast.makeText(context, "Erreur d'initialisation de youtube player.", Toast.LENGTH_LONG).show();
            }
        });
    }


    @Override
    public int getItemCount() {
        return videosList == null ? 0 : videosList.size();
    }

    public class MovieVideoHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.movie_video_view)
        YouTubePlayerView movie_video_view;

        public MovieVideoHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }

    }
}
