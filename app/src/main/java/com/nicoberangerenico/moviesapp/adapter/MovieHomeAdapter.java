package com.nicoberangerenico.moviesapp.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nicoberangerenico.moviesapp.R;
import com.nicoberangerenico.movieslib.Models.Enums.PictureSize;
import com.nicoberangerenico.movieslib.Models.Movie;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MovieHomeAdapter extends RecyclerView.Adapter<MovieHomeAdapter.ViewHolder> {

    private List<Movie> movieList;
    private Context context;
    private Listener listener;

    public MovieHomeAdapter(Context context, List<Movie> movieList) {
        this.movieList = movieList;
        this.context = context;
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public MovieHomeAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.item_movie_home, parent, false);
        return new MovieHomeAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MovieHomeAdapter.ViewHolder holder, int position) {
        final Movie movie = movieList.get(position);

        if(movie.getPosterPath().compareTo("") != 0)
            Picasso.get()
                    .load(movie.getFullPosterPath(PictureSize.POSTER_342.toString()))
                    .into(holder.imgCover);

        holder.txtTitle.setText(movie.getTitle());
        holder.txtGenre.setText(movie.getGenresStringFormat(this.context));
        holder.txtRate.setText(String.format("%s ★", new DecimalFormat("0.0").format(movie.getVoteAverage()/2)));

        holder.setIDMovie(movie.getId());


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) listener.onMovieClick(movie);
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.movieList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.movie_cover) ImageView imgCover;
        @BindView(R.id.movie_title) TextView txtTitle;
        @BindView(R.id.movie_genre) TextView txtGenre;
        @BindView(R.id.movie_rate) TextView txtRate;

        private long IDMovie;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public long getIDMovie() {
            return IDMovie;
        }

        public void setIDMovie(long IDMovie) {
            this.IDMovie = IDMovie;
        }
    }

    public interface Listener {
        void onMovieClick(Movie movie);
    }
}
