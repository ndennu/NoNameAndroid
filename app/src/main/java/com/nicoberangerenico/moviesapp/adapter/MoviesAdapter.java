package com.nicoberangerenico.moviesapp.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.zagum.switchicon.SwitchIconView;
import com.nicoberangerenico.moviesapp.R;
import com.nicoberangerenico.moviesapp.tools.ConstFile;
import com.nicoberangerenico.movieslib.API.IApiListner;
import com.nicoberangerenico.movieslib.API.MoviesProvider;
import com.nicoberangerenico.movieslib.Models.Genre;
import com.nicoberangerenico.movieslib.Models.Movie;
import com.nicoberangerenico.movieslib.SQLite.DatabaseAccess;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MoviesAdapter extends RecyclerView.Adapter<MoviesAdapter.MoviesViewHolder> {

    // [INFO] - variable sorted by Asc name
    private Context context;
    private List<Movie> moviesList;
    private Listener listener;
    private DatabaseAccess databaseAccess;
    //private boolean isLogged;

    public MoviesAdapter(Context context, List<Movie> moviesList) {
        this.context = context;
        this.moviesList = moviesList;
        this.databaseAccess = DatabaseAccess.getInstance(context);
        //SharedPreferences sharedPreferences = context.getSharedPreferences(ConstFile.SHARED_PREF_LOGIN, Context.MODE_PRIVATE);
        //isLogged = sharedPreferences.getBoolean(ConstFile.SHARED_PREF_LOGIN, false);
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }
    public void setMoviesList(List<Movie> moviesList) {
        this.moviesList = moviesList;
    }

    @NonNull
    @Override
    public MoviesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View moviesCell = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_movies, parent, false);
        return new MoviesViewHolder(moviesCell);
    }

    @Override
    public void onBindViewHolder(@NonNull final MoviesViewHolder holder, int position) {

        final Movie movies = moviesList.get(position);

        if(movies.getBackdropPath() != null && movies.getPosterPath().compareTo("") != 0)
            Picasso.get()
                    .load("https://image.tmdb.org/t/p/" + "w185" + movies.getPosterPath())
                    .into(holder.imageViewMovies);

        holder.nameMoviesLabel.setText(movies.getTitle());

        holder.noteMoviesLabel.setText(String.format("%s ★", Float.toString(movies.getVoteAverage())));

        holder.categoryMoviesLabel.setText(setCategoryLabel(movies.getTabGenreIds()));

        holder.setIDMovie(movies.getId());

        //final DatabaseAccess access = DatabaseAccess.getInstance(context);
        /*if (access.movieExist(movies.getId())) {
            holder.imageViewLike.setIconEnabled(true);
        }*/

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) listener.onMovieClick(movies);
            }
        });

        /*if (!isLogged) {
            holder.imageViewLike.setVisibility(View.GONE);
        }*/

        /*holder.imageViewLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (access.movieExist(movies.getId())) {
                    access.deleteMovie(movies.getId());
                    holder.imageViewLike.switchState();
                }
                else {
                    MoviesProvider.getINSTANCE().getMovieDetails(movies.getId(), new IApiListner<Movie>() {
                        @Override
                        public void onSuccess(Movie response) {
                            access.insertMovie(response);
                            holder.imageViewLike.switchState();
                        }

                        @Override
                        public void onError(String s) {
                            Toast.makeText(context, "Error while insert movie in database", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });*/
    }

    private String setCategoryLabel(int[] categoryTab) {
        List<Genre> categoryList = databaseAccess.getAllGenre();

        StringBuilder category = new StringBuilder("");

        // TODO: SIMPLIFY THIS LOOP
        for (int aCategoryTab : categoryTab) {
            for (int j = 0; j < categoryList.size(); j++) {
                if (categoryList.get(j).getId() == aCategoryTab) {
                    if (!category.toString().equals(""))
                        category.append("\t");
                    category.append(categoryList.get(j).getName());
                    break;
                }
            }
        }
        return category.toString();
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }

    public class MoviesViewHolder extends RecyclerView.ViewHolder {

        // [INFO] - variable sorted by Asc name
        //@BindView(R.id.movies_card) View cardView;
        @BindView(R.id.movies_category) TextView categoryMoviesLabel;
        //@BindView(R.id.movies_image_like) SwitchIconView imageViewLike;
        @BindView(R.id.movies_image_film) ImageView imageViewMovies;
        @BindView(R.id.movies_name_years) TextView nameMoviesLabel;
        @BindView(R.id.movies_note) TextView noteMoviesLabel;

        private long IDMovie;

        public MoviesViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public long getIDMovie() {
            return IDMovie;
        }

        public void setIDMovie(long IDMovie) {
            this.IDMovie = IDMovie;
        }
    }

    public interface Listener {
        void onMovieClick(Movie movie);
    }
}
