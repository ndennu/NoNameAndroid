package com.nicoberangerenico.moviesapp.tools;

public class ConstFile {
    public static final String SHARED_PREF_LOGIN = "LOGGED_IN";
    public static final String FIREBASE_NODE_MOVIE = "movies";
}
