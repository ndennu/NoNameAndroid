package com.nicoberangerenico.movieslib.API;

public interface IApiListner<T> {
    void onSuccess(T response);
    void onError(String s);
}
