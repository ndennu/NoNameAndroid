package com.nicoberangerenico.movieslib.API;

import com.nicoberangerenico.movieslib.Models.GenreResult;
import com.nicoberangerenico.movieslib.Models.Movie;
import com.nicoberangerenico.movieslib.Models.MovieVideos;
import com.nicoberangerenico.movieslib.Models.MoviesResult;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface IMoviesService {

    @GET("movie/popular") Call<MoviesResult> getMoviesListPopular(
            @Query("api_key") String api_key,
            @Query("language") String language,
            @Query("page") int page);

    @GET("search/movie") Call<MoviesResult> getMoviesListSearch(
            @Query("api_key") String api_key,
            @Query("language") String language,
            @Query("query") String query,
            @Query("page") int page);

    @GET("movie/upcoming") Call<MoviesResult> getMoviesListUpComing(
            @Query("api_key") String api_key,
            @Query("language") String language,
            @Query("page") int page);

    @GET("genre/movie/list") Call<GenreResult> getGenreList(
            @Query("api_key") String api_key,
            @Query("language") String language);

    @GET("movie/{movie_id}") Call<Movie> getMovieDetails(
            @Path("movie_id") long id,
            @Query("api_key") String api_key,
            @Query("language") String language);

    @GET("movie/{movie_id}/similar") Call<MoviesResult> getSimilarMovies(
            @Path("movie_id") long id,
            @Query("api_key") String api_key,
            @Query("language") String language);

    @GET("movie/{movie_id}/videos") Call<MovieVideos> getMovieVideos(
            @Path("movie_id") long id,
            @Query("api_key") String api_key,
            @Query("language") String language);

    @GET("genre/{genre_id}/movies") Call<MoviesResult> getMovieWithGender(
            @Path("genre_id") int id,
            @Query("api_key") String api_key,
            @Query("language") String language,
            @Query("page") int page);
}
