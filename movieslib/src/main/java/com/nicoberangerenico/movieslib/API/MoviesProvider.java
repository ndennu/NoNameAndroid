package com.nicoberangerenico.movieslib.API;

import android.content.Context;
import android.util.Log;

import com.nicoberangerenico.movieslib.Models.GenreResult;
import com.nicoberangerenico.movieslib.Models.Movie;
import com.nicoberangerenico.movieslib.Models.MovieVideos;
import com.nicoberangerenico.movieslib.Models.MoviesResult;
import com.nicoberangerenico.movieslib.R;
import com.nicoberangerenico.movieslib.SQLite.DatabaseAccess;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MoviesProvider {

    // [INFO] - variable sorted by Asc name
    private Context context = null;
    private static MoviesProvider INSTANCE = null;
    private IMoviesService iMoviesService;


    private MoviesProvider() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.themoviedb.org/3/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(createOkHttpClient())
                .build();
        iMoviesService = retrofit.create(IMoviesService.class);
    }

    // synchronized = pour les threads
    public static synchronized MoviesProvider getINSTANCE() {
        if (INSTANCE == null)
            INSTANCE = new MoviesProvider();
        return INSTANCE;
    }

    public void setContext(Context context) {
        if (this.context == null)
            this.context = context;
    }

    private OkHttpClient createOkHttpClient() {
        OkHttpClient.Builder okBuilder = new OkHttpClient.Builder();
        return okBuilder.build();
    }

    public void getMoviesListUpComing(int page, final IApiListner<MoviesResult> listner) {
        iMoviesService.getMoviesListUpComing(context.getString(R.string.api_key), context.getString(R.string.language), page).enqueue(new Callback<MoviesResult>() {
            @Override
            public void onResponse(Call<MoviesResult> call, Response<MoviesResult> response) {
                if (response.code() != 200) {
                    Log.e("(GET) MOVIES_LIST_UP",  String.format("CODE_RETURN: %d, MESSAGE: %s", response.code(), response.message()));
                    listner.onError("ERROR GET ALL UP COMMING MOVIES LIST");
                    return;
                }
                MoviesResult moviesResult = response.body();
                Log.i("(GET) MOVIES_LIST_UP",  String.format("CODE_RETURN: %d, MESSAGE: %s", response.code(), response.message()));
                listner.onSuccess(moviesResult);
            }

            @Override
            public void onFailure(Call<MoviesResult> call, Throwable t) {
                Log.e("MOVIES_LIST_UP", "ERROR WEB SERVICES\n\n" + t.getMessage());
                listner.onError(t.getMessage());
            }
        });
    }

    public void getMoviesListPopular(int page, final IApiListner<MoviesResult> listner) {
        iMoviesService.getMoviesListPopular(context.getString(R.string.api_key), context.getString(R.string.language), page).enqueue(new Callback<MoviesResult>() {
            @Override
            public void onResponse(Call<MoviesResult> call, Response<MoviesResult> response) {
                if (response.code() != 200) {
                    Log.e("(GET) MOVIES_LIST_POP",  String.format("CODE_RETURN: %d, MESSAGE: %s", response.code(), response.message()));
                    listner.onError("ERROR GET ALL POPULAR MOVIES LIST");
                    return;
                }
                MoviesResult moviesResult = response.body();
                Log.i("(GET) MOVIES_LIST_POP",  String.format("CODE_RETURN: %d, MESSAGE: %s", response.code(), response.message()));
                listner.onSuccess(moviesResult);
            }

            @Override
            public void onFailure(Call<MoviesResult> call, Throwable t) {
                Log.e("MOVIES_LIST_POP", "ERROR WEB SERVICES\n\n" + t.getMessage());
                listner.onError(t.getMessage());
            }
        });
    }

    public void getMoviesListSearch(String query, int page, final IApiListner<MoviesResult> listner) {
        iMoviesService.getMoviesListSearch(context.getString(R.string.api_key), context.getString(R.string.language), query, page).enqueue(new Callback<MoviesResult>() {
            @Override
            public void onResponse(Call<MoviesResult> call, Response<MoviesResult> response) {
                if (response.code() != 200) {
                    Log.e("(GET) SEARCH_MOVIES",  String.format("CODE_RETURN: %d, MESSAGE: %s", response.code(), response.message()));
                    listner.onError("ERROR SEARCH");
                    return;
                }
                MoviesResult moviesResult = response.body();
                Log.i("(GET) SEARCH_MOVIES",  String.format("CODE_RETURN: %d, MESSAGE: %s", response.code(), response.message()));
                listner.onSuccess(moviesResult);
            }

            @Override
            public void onFailure(Call<MoviesResult> call, Throwable t) {
                Log.e("SEARCH_MOVIES", "ERROR WEB SERVICES\n\n" + t.getMessage());
                listner.onError(t.getMessage());
            }
        });
    }

    public void getMovieDetails(long id, final IApiListner<Movie> listner) {
        iMoviesService.getMovieDetails(id, context.getString(R.string.api_key), context.getString(R.string.language)).enqueue(new Callback<Movie>() {
            @Override
            public void onResponse(Call<Movie> call, Response<Movie> response) {
                if (response.code() != 200) {
                    Log.e("(GET) MOVIES_DETAILS",  String.format("CODE_RETURN: %d, MESSAGE: %s", response.code(), response.message()));
                    listner.onError("ERROR GET ONE MOVIE");
                    return;
                }
                Movie movieDetails = response.body();
                Log.i("(GET) MOVIES_DETAILS",  String.format("CODE_RETURN: %d, MESSAGE: %s", response.code(), response.message()));
                listner.onSuccess(movieDetails);
            }

            @Override
            public void onFailure(Call<Movie> call, Throwable t) {
                Log.e("(GET) MOVIES_DETAILS", "ERROR WEB SERVICES\n\n" + t.getMessage());
                listner.onError(t.getMessage());
            }
        });
    }

    public void getGenres(final IApiListner<GenreResult> listner) {
        iMoviesService.getGenreList(context.getString(R.string.api_key), context.getString(R.string.language)).enqueue(new Callback<GenreResult>() {
            @Override
            public void onResponse(Call<GenreResult> call, Response<GenreResult> response) {
                if (response.code() != 200) {
                    Log.e("(GET) CATEGORY_LIST",  String.format("CODE_RETURN: %d, MESSAGE: %s", response.code(), response.message()));
                    listner.onError("ERROR GET ALL CATEGORY");
                    return;
                }
                DatabaseAccess databaseAccess = DatabaseAccess.getInstance(context);
                Log.i("(GET) CATEGORY_LIST",  String.format("CODE_RETURN: %d, MESSAGE: %s", response.code(), response.message()));
                GenreResult genreResult = response.body();
                listner.onSuccess(genreResult);
            }

            @Override
            public void onFailure(Call<GenreResult> call, Throwable t) {
                listner.onError(t.getMessage());
            }
        });
    }

    public void getSimilarMovies(long id, final IApiListner<MoviesResult> listner) {
        iMoviesService.getSimilarMovies(id, context.getString(R.string.api_key), context.getString(R.string.language)).enqueue(new Callback<MoviesResult>() {
            @Override
            public void onResponse(Call<MoviesResult> call, Response<MoviesResult> response) {
                if (response.code() != 200) {
                    Log.e("(GET) SIMILAR_MOVIE",  String.format("CODE_RETURN: %d, MESSAGE: %s", response.code(), response.message()));
                }
                Log.i("(GET) SIMILAR_MOVIE",  String.format("CODE_RETURN: %d, MESSAGE: %s", response.code(), response.message()));
                MoviesResult moviesResult = response.body();
                listner.onSuccess(moviesResult);
            }

            @Override
            public void onFailure(Call<MoviesResult> call, Throwable t) {
                Log.e("(GET) MOVIES_SIMILARY", "ERROR WEB SERVICES\n\n" + t.getMessage());
                listner.onError(t.getMessage());
            }
        });
    }

    public void getMovieVideos(long id, final IApiListner<MovieVideos> listner) {
        iMoviesService.getMovieVideos(id, context.getString(R.string.api_key), context.getString(R.string.language)).enqueue(new Callback<MovieVideos>() {
            @Override
            public void onResponse(Call<MovieVideos> call, Response<MovieVideos> response) {
                if (response.code() != 200) {
                    Log.e("(GET) MOVIE_VIDEO",  String.format("CODE_RETURN: %d, MESSAGE: %s", response.code(), response.message()));
                    listner.onError("ERROR MOVIE VIDEO");
                    return;
                }
                Log.i("(GET) MOVIE_VIDEO",  String.format("CODE_RETURN: %d, MESSAGE: %s", response.code(), response.message()));
                MovieVideos movieVideos = response.body();
                listner.onSuccess(movieVideos);
            }

            @Override
            public void onFailure(Call<MovieVideos> call, Throwable t) {
                Log.e("(GET) MOVIES_VIDEOS", "ERROR WEB SERVICES\n\n" + t.getMessage());
                listner.onError(t.getMessage());
            }
        });
    }

    public void getMoviesWithGender(int id, int page, final IApiListner<MoviesResult> listner) {
        iMoviesService.getMovieWithGender(id, context.getString(R.string.api_key), context.getString(R.string.language), page).enqueue(new Callback<MoviesResult>() {
            @Override
            public void onResponse(Call<MoviesResult> call, Response<MoviesResult> response) {
                if (response.code() != 200) {
                    Log.e("(GET) MOVIE_W_GENDER",  String.format("CODE_RETURN: %d, MESSAGE: %s", response.code(), response.message()));
                    listner.onError("ERROR MOVIES WITH GENDER");
                    return;
                }
                Log.i("(GET) MOVIE_W_GENDER",  String.format("CODE_RETURN: %d, MESSAGE: %s", response.code(), response.message()));
                MoviesResult moviesResult = response.body();
                listner.onSuccess(moviesResult);
            }

            @Override
            public void onFailure(Call<MoviesResult> call, Throwable t) {
                listner.onError(t.getMessage());
            }
        });
    }

}
