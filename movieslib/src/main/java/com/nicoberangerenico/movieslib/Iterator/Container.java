package com.nicoberangerenico.movieslib.Iterator;

public interface Container {
    public Iterator getIterator();
}
