package com.nicoberangerenico.movieslib.Iterator;

public interface Iterator {
    public boolean hasNext();
    public Object next();
}
