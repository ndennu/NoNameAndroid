package com.nicoberangerenico.movieslib.Models.Enums;

public enum ExtraParam {
    DEFAULT(0),
    POPULAR(1),
    UP_COMING(2);

    private int param;

    ExtraParam(int param) {
        this.param = param;
    }

    public int value() {
        return param;
    }
}
