package com.nicoberangerenico.movieslib.Models.Enums;

public enum PictureSize {
    BACKDROP_780("w780"),
    BACKDROP_1280("w1280"),
    BACKDROP_ORIGINAL("original"),
    POSTER_154("w154"),
    POSTER_185("w185"),
    POSTER_342("w342");

    private String fullName;

    PictureSize(String s) {
        this.fullName = s;
    }

    @Override
    public String toString() {
        return fullName;
    }

}
