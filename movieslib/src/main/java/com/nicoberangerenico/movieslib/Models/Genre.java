package com.nicoberangerenico.movieslib.Models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Genre implements Serializable {

    @SerializedName("id") private int id;
    @SerializedName("name") private String name;

    public Genre(){}

    private Genre(Genre.Builder builder) {
        this.id = builder.id;
        this.name = builder.name;
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }


    public static class Builder {

        private int id;
        private String name;


        public Builder id(int id){
            this.id = id;
            return this;
        }

        public Builder name(String name){
            this.name = name;
            return this;
        }


        public Genre build() {
            return new Genre(this);
        }
    }
}
