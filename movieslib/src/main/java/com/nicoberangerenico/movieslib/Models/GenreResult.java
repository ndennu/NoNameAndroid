package com.nicoberangerenico.movieslib.Models;

import com.google.gson.annotations.SerializedName;

import java.util.Iterator;
import java.util.List;

public class GenreResult {

    @SerializedName("genres") private List<Genre> genreList;

    public List<Genre> getGenreList() {
        return genreList;
    }
    public void setGenreList(List<Genre> genreList) {
        this.genreList = genreList;
    }

    @Override
    public String toString() {
        return "GenreResult{" +
                "genreList=" + genreList +
                '}';
    }

    public Iterator<Genre> iterator() {
        return new GenreResultIterator();
    }

    private class GenreResultIterator implements Iterator<Genre> {

        int index;

        public GenreResultIterator(){}

        @Override
        public boolean hasNext() {
            if (index < genreList.size())
                return true;
            return false;
        }

        @Override
        public Genre next() {
            if (this.hasNext())
                return genreList.get(index++);
            return null;
        }
    }
}