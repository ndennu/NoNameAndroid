package com.nicoberangerenico.movieslib.Models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class LanguagesMovie implements Serializable{

    @SerializedName("iso_639_1") private String iso;
    private String name;

    public LanguagesMovie(){}

    private LanguagesMovie(Builder builder) {
        setIso(builder.iso);
        setName(builder.name);
    }

    public String getIso() {
        return iso;
    }
    public void setIso(String iso) {
        this.iso = iso;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "LanguagesMovie{" +
                "iso='" + iso + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
    
    public static final class Builder {
        private String iso;
        private String name;

        public Builder() {
        }

        public Builder iso(String val) {
            iso = val;
            return this;
        }

        public Builder name(String val) {
            name = val;
            return this;
        }

        public LanguagesMovie build() {
            return new LanguagesMovie(this);
        }
    }
}
