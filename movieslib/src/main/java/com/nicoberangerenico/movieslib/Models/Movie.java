package com.nicoberangerenico.movieslib.Models;

import android.content.Context;

import com.google.gson.annotations.SerializedName;
import com.nicoberangerenico.movieslib.SQLite.DatabaseAccess;

import java.io.Serializable;
import java.util.List;

public class Movie implements Serializable {

    @SerializedName("adult") private boolean adult;
    @SerializedName("backdrop_path") private String backdrop_path;
    @SerializedName("budget") private long budget;
    @SerializedName("genre_ids") private int[] tab_genre_ids;
    @SerializedName("genres") private List<Genre> genres_list;
    @SerializedName("homepage") private String homepage;
    @SerializedName("id") private long id;
    @SerializedName("original_language") private String original_language;
    @SerializedName("original_title") private String original_title;
    @SerializedName("overview") private String overview;
    @SerializedName("popularity") private float popularity;
    @SerializedName("poster_path") private String poster_path;
    @SerializedName("production_companies") private List<ProductionCompanies> production_companies;
    @SerializedName("production_countries") private List<ProductionCountries> production_countries;
    @SerializedName("release_date") private String release_date;
    @SerializedName("revenue") private long revenue;
    @SerializedName("runtime") private int runtime;
    @SerializedName("spoken_languages") private List<LanguagesMovie> spoken_languages;
    @SerializedName("status") private String status;
    @SerializedName("tagline") private String tagline;
    @SerializedName("title") private String title;
    @SerializedName("video") private boolean video;
    @SerializedName("vote_average") private float vote_average;
    @SerializedName("vote_count") private long vote_count;

    public Movie(){}

    private Movie(Movie.Builder builder) {
        this.adult = builder.adult;
        this.backdrop_path = builder.backdrop_path;
        this.budget = builder.budget;
        this.tab_genre_ids = builder.tab_genre_ids;
        this.genres_list = builder.genres_list;
        this.homepage = builder.homepage;
        this.id = builder.id;
        this.original_language = builder.original_language;
        this.original_title = builder.original_title;
        this.overview = builder.overview;
        this.popularity = builder.popularity;
        this.poster_path = builder.poster_path;
        this.production_companies = builder.production_companies;
        this.production_countries = builder.production_countries;
        this.release_date = builder.release_date;
        this.revenue = builder.revenue;
        this.runtime = builder.runtime;
        this.spoken_languages = builder.spoken_languages;
        this.status = builder.status;
        this.tagline = builder.tagline;
        this.title = builder.title;
        this.video = builder.video;
        this.vote_average = builder.vote_average;
        this.vote_count = builder.vote_count;
    }


    public List<ProductionCompanies> getProductionCompanies() {
        return production_companies;
    }
    public void setProductionCompanies(List<ProductionCompanies> production_companies) {
        this.production_companies = production_companies;
    }

    public List<ProductionCountries> getProductionCountries() {
        return production_countries;
    }
    public void setProductionCountries(List<ProductionCountries> production_countries) {
        this.production_countries = production_countries;
    }

    public String getHomepage() {
        return homepage;
    }
    public void setHomepage(String homepage) {
        this.homepage = homepage;
    }

    public long getBudget() {
        return budget;
    }
    public void setBudget(long budget) {
        this.budget = budget;
    }

    public long getVoteCount() {
        return vote_count;
    }
    public void setVoteCount(long vote_count) {
        this.vote_count = vote_count;
    }

    public boolean isVideo() {
        return video;
    }
    public void setVideo(boolean video) {
        this.video = video;
    }

    public float getPopularity() {
        return popularity;
    }
    public void setPopularity(float popularity) {
        this.popularity = popularity;
    }

    public String getOriginalLanguage() {
        return original_language;
    }
    public void setOriginalLanguage(String original_language) {
        this.original_language = original_language;
    }

    public String getOriginalTitle() {
        return original_title;
    }
    public void setOriginalTitle(String original_title) {
        this.original_title = original_title;
    }

    public List<Genre> getGenresList() {
        return genres_list;
    }
    public void setGenresList(List<Genre> genres_list) {
        this.genres_list = genres_list;
    }

    public boolean isAdult() {
        return adult;
    }
    public void setAdult(boolean adult) {
        this.adult = adult;
    }

    public long getRevenue() {
        return revenue;
    }
    public void setRevenue(long revenue) {
        this.revenue = revenue;
    }

    public int getRuntime() {
        return runtime;
    }
    public void setRuntime(int runtime) {
        this.runtime = runtime;
    }

    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }

    public String getTagline() {
        return tagline;
    }
    public void setTagline(String tagline) {
        this.tagline = tagline;
    }

    public List<LanguagesMovie> getSpokenLanguages() {
        return spoken_languages;
    }
    public void setSpokenLanguages(List<LanguagesMovie> spoken_languages) {
        this.spoken_languages = spoken_languages;
    }

    public int[] getTabGenreIds() {
        return tab_genre_ids;
    }
    public void setTabGenreIds(int[] tab_genre_ids) {
        this.tab_genre_ids = tab_genre_ids;
    }

    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public String getOverview() {
        return overview;
    }
    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getBackdropPath() {
        return backdrop_path;
    }
    public void setBackdropPath(String backdrop_path) {
        this.backdrop_path = backdrop_path;
    }

    public String getFullBackdropPath(String size){
        return "https://image.tmdb.org/t/p/" + size + backdrop_path;
    }

    public String getPosterPath() {
        return poster_path;
    }
    public void setPosterPath(String poster_path) {
        this.poster_path = poster_path;
    }

    public String getFullPosterPath(String size){
        return "https://image.tmdb.org/t/p/" + size + poster_path;
    }

    public float getVoteAverage() {
        return vote_average;
    }
    public void setVoteAverage(float vote_average) {
        this.vote_average = vote_average;
    }

    public String getReleaseDate() {
        return release_date;
    }
    public void setReleaseDate(String release_date) {
        this.release_date = release_date;
    }


    public String getGenresStringFormat(Context context){
        StringBuilder sb = new StringBuilder();

        if(genres_list != null){
            for(int i = 0; i < genres_list.size(); i++){
                sb.append(genres_list.get(i).getName());
                if(i != genres_list.size() - 1)
                    sb.append(", ");
            }
        } else if(tab_genre_ids != null) {
            DatabaseAccess databaseAccess = DatabaseAccess.getInstance(context);

            for (int i = 0; i < tab_genre_ids.length; i++){
                sb.append(databaseAccess.getGenreById(tab_genre_ids[i]).getName());
                if(i != tab_genre_ids.length - 1){
                    sb.append(", ");
                }
            }
        }

        return sb.toString();
    }


    public static class Builder {

        private boolean adult;
        private String backdrop_path;
        private long budget;
        private List<Genre> genres_list;
        private String homepage;
        private long id;
        private String original_language;
        private String original_title;
        private String overview;
        private float popularity;
        private String poster_path;
        private List<ProductionCompanies> production_companies;
        private List<ProductionCountries> production_countries;
        private String release_date;
        private long revenue;
        private int runtime;
        private List<LanguagesMovie> spoken_languages;
        private String status;
        private int[] tab_genre_ids;
        private String tagline;
        private String title;
        private boolean video;
        private long vote_count;
        private float vote_average;


        public Builder adult(boolean adult){
            this.adult = adult;
            return this;
        }

        public Builder backdropPath(String backdrop_path){
            this.backdrop_path = backdrop_path;
            return this;
        }

        public Builder budget(long budget){
            this.budget = budget;
            return this;
        }

        public Builder genresList(List<Genre> genres_list){
            this.genres_list = genres_list;
            return this;
        }

        public Builder homepage(String homepage){
            this.homepage = homepage;
            return this;
        }

        public Builder id(long id){
            this.id = id;
            return this;
        }

        public Builder originalLanguage(String original_language){
            this.original_language = original_language;
            return this;
        }

        public Builder originalTitle(String original_title){
            this.original_title = original_title;
            return this;
        }

        public Builder overview(String overview){
            this.overview = overview;
            return this;
        }

        public Builder popularity(float popularity){
            this.popularity = popularity;
            return this;
        }

        public Builder posterPath(String poster_path){
            this.poster_path = poster_path;
            return this;
        }

        public Builder productionCompagnies(List<ProductionCompanies> production_companies){
            this.production_companies = production_companies;
            return this;
        }

        public Builder productionCountries(List<ProductionCountries> production_countries){
            this.production_countries =production_countries;
            return this;
        }

        public Builder releaseDate(String release_date){
            this.release_date = release_date;
            return this;
        }

        public Builder revenue(long revenue){
            this.revenue = revenue;
            return this;
        }

        public Builder runtime(int runtime){
            this.runtime = runtime;
            return this;
        }

        public Builder spokenLanguages(List<LanguagesMovie> spoken_languages){
            this.spoken_languages = spoken_languages;
            return this;
        }

        public Builder status(String status){
            this.status = status;
            return this;
        }

        public Builder tabGenreIds(int[] tab_genre_ids){
            this.tab_genre_ids = tab_genre_ids;
            return this;
        }

        public Builder tagline(String tagline){
            this.tagline = tagline;
            return this;
        }

        public Builder title(String title){
            this.title = title;
            return this;
        }

        public Builder video(boolean video){
            this.video = video;
            return this;
        }

        public Builder voteAverage(float vote_average){
            this.vote_average = vote_average;
            return this;
        }

        public Builder voteCount(long vote_count){
            this.vote_count = vote_count;
            return this;
        }


        public Movie build(){
            return new Movie(this);
        }
    }
}
