package com.nicoberangerenico.movieslib.Models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MovieVideos {

    @SerializedName("id") private long id;
    @SerializedName("results") private List<Videos> videos_list;

    public MovieVideos(long id, List<Videos> videos_list) {
        this.id = id;
        this.videos_list = videos_list;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public List<Videos> getVideos_list() {
        return videos_list;
    }

    public void setVideos_list(List<Videos> videos_list) {
        this.videos_list = videos_list;
    }

    @Override
    public String toString() {
        return "MovieVideos{" +
                "id=" + id +
                ", videos_list=" + videos_list +
                '}';
    }
}
