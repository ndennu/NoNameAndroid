package com.nicoberangerenico.movieslib.Models;

import com.google.gson.annotations.SerializedName;

import java.util.Iterator;
import java.util.List;

public class MoviesResult {

    // [INFO] - variable sorted by Asc name
    @SerializedName("next") public String next;
    @SerializedName("previous") public String previous;
    @SerializedName("results") public List<Movie> moviesList;

    public String getPrevious() {
        return previous;
    }
    public void setPrevious(String previous) {
        this.previous = previous;
    }

    public String getNext() {
        return next;
    }
    public void setNext(String next) {
        this.next = next;
    }

    public List<Movie> getMoviesList() {
        return moviesList;
    }
    public void setMoviesList(List<Movie> moviesList) {
        this.moviesList = moviesList;
    }

    @Override
    public String toString() {
        return "MoviesResult{" +
                "previous='" + previous + '\'' +
                ", next='" + next + '\'' +
                ", moviesList=" + moviesList +
                '}';
    }

    public Iterator<Movie> iterator() {
        return new MovieResultIterator();
    }

    private class MovieResultIterator implements Iterator<Movie> {

        int index;

        public MovieResultIterator(){}

        @Override
        public boolean hasNext() {
            if (index < moviesList.size())
                return true;
            return false;
        }

        @Override
        public Movie next() {
            if (this.hasNext())
                return moviesList.get(index++);
            return null;
        }
    }
}
