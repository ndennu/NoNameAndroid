package com.nicoberangerenico.movieslib.Models;

import java.io.Serializable;

public class ProductionCompanies implements Serializable{

    private int id;
    private String logo_path;
    private String name;
    private String origin_country;

    public ProductionCompanies(){}

    private ProductionCompanies(Builder builder) {
        setId(builder.id);
        setLogo_path(builder.logo_path);
        setName(builder.name);
        setOrigin_country(builder.origin_country);
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public String getLogo_path() {
        return logo_path;
    }
    public void setLogo_path(String logo_path) {
        this.logo_path = logo_path;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getOrigin_country() {
        return origin_country;
    }
    public void setOrigin_country(String origin_country) {
        this.origin_country = origin_country;
    }

    @Override
    public String toString() {
        return "ProductionCompanies{" +
                "id=" + id +
                ", logo_path='" + logo_path + '\'' +
                ", name='" + name + '\'' +
                ", origin_country='" + origin_country + '\'' +
                '}';
    }

    public static final class Builder {
        private int id;
        private String logo_path;
        private String name;
        private String origin_country;

        public Builder() {
        }

        public Builder id(int val) {
            id = val;
            return this;
        }

        public Builder logo_path(String val) {
            logo_path = val;
            return this;
        }

        public Builder name(String val) {
            name = val;
            return this;
        }

        public Builder origin_country(String val) {
            origin_country = val;
            return this;
        }

        public ProductionCompanies build() {
            return new ProductionCompanies(this);
        }
    }
}
