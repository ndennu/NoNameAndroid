package com.nicoberangerenico.movieslib.Models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ProductionCountries implements Serializable {

    @SerializedName("iso_3166_1") private String iso;
    private String name;

    public ProductionCountries(){}

    private ProductionCountries(Builder builder) {
        setIso(builder.iso);
        setName(builder.name);
    }

    public String getIso() {
        return iso;
    }
    public void setIso(String iso) {
        this.iso = iso;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "ProductionCountries{" +
                "iso='" + iso + '\'' +
                ", name='" + name + '\'' +
                '}';
    }


    public static final class Builder {
        private String iso;
        private String name;

        public Builder() {
        }

        public Builder iso(String val) {
            iso = val;
            return this;
        }

        public Builder name(String val) {
            name = val;
            return this;
        }

        public ProductionCountries build() {
            return new ProductionCountries(this);
        }
    }
}
