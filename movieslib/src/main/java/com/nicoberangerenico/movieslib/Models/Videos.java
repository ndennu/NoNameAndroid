package com.nicoberangerenico.movieslib.Models;

import com.google.gson.annotations.SerializedName;

public class Videos {

    @SerializedName("id") private String id;
    @SerializedName("iso_639_1") private String iso_639;
    @SerializedName("iso_3166_1") private String iso_3166;
    @SerializedName("key") private String key;
    @SerializedName("name") private String name;
    @SerializedName("site") private String site;
    @SerializedName("size") private long size;
    @SerializedName("type") private String type;

    public Videos(String id, String iso_639, String iso_3166, String key, String name, String site, long size, String type) {
        this.id = id;
        this.iso_639 = iso_639;
        this.iso_3166 = iso_3166;
        this.key = key;
        this.name = name;
        this.site = site;
        this.size = size;
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIso_639() {
        return iso_639;
    }

    public void setIso_639(String iso_639) {
        this.iso_639 = iso_639;
    }

    public String getIso_3166() {
        return iso_3166;
    }

    public void setIso_3166(String iso_3166) {
        this.iso_3166 = iso_3166;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Videos{" +
                "id='" + id + '\'' +
                ", iso_639='" + iso_639 + '\'' +
                ", iso_3166='" + iso_3166 + '\'' +
                ", key='" + key + '\'' +
                ", name='" + name + '\'' +
                ", site='" + site + '\'' +
                ", size=" + size +
                ", type='" + type + '\'' +
                '}';
    }
}
