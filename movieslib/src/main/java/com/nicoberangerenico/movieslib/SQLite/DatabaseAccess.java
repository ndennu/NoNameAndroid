package com.nicoberangerenico.movieslib.SQLite;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.nicoberangerenico.movieslib.Models.Genre;
import com.nicoberangerenico.movieslib.Models.LanguagesMovie;
import com.nicoberangerenico.movieslib.Models.Movie;
import com.nicoberangerenico.movieslib.Models.ProductionCompanies;
import com.nicoberangerenico.movieslib.Models.ProductionCountries;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DatabaseAccess {

    private static DatabaseAccess instance;

    private static final String DATABASE_NAME = "com.nicoberangerenico.movies.sqlite";
    private int DATABASE_VERSION = 1;

    private MySQLiteOpenHelper mySQLiteOpenHelper;
    private SQLiteDatabase db;


    private DatabaseAccess(Context context){
        mySQLiteOpenHelper = new MySQLiteOpenHelper(context, DATABASE_NAME, DATABASE_VERSION);
    }

    public static DatabaseAccess getInstance(Context context){
        if(instance == null){
            instance = new DatabaseAccess(context);
        }

        return instance;
    }

    /**
     * Insert a movie in database
     * @param movie Movie to insert
     */
    public void insertMovie(Movie movie){
        db = mySQLiteOpenHelper.getWritableDatabase();

        StringBuilder sb = new StringBuilder();

        sb.append("INSERT INTO movie (id, title, overview, backdrop_path, poster_path, vote_average, release_date, adult, budget, homepage, original_language, original_title, popularity, revenue, runtime, status, tagline, video, vote_count) VALUES (")
                .append(Long.toString(movie.getId()))
                .append(", \"").append(movie.getTitle()).append("\", ")
                .append("\"").append(movie.getOverview().replace("\"", "'")).append("\", ")
                .append("\"").append(movie.getBackdropPath()).append("\", ")
                .append("\"").append(movie.getPosterPath()).append("\", ")
                .append(movie.getVoteAverage()).append(", ")
                .append(" \"").append(movie.getReleaseDate()).append("\", ")
                .append(movie.isAdult() ? "1" : "0").append(", ")
                .append(movie.getBudget()).append(", ")
                .append("\"").append(movie.getHomepage()).append("\", ")
                .append("\"").append(movie.getOriginalLanguage()).append("\", ")
                .append("\"").append(movie.getOriginalTitle()).append("\", ")
                .append(movie.getPopularity()).append(", ")
                .append(movie.getRevenue()).append(", ")
                .append(movie.getRuntime()).append(", ")
                .append("\"").append(movie.getStatus()).append("\", ")
                .append("\"").append(movie.getTagline()).append("\", ")
                .append(movie.isVideo() ? "1" : "0").append(", ")
                .append(movie.getVoteCount()).append(");");

        db.execSQL(sb.toString());

        insertMovieGenre(movie.getId(), movie.getGenresList());
        insertProductionCompany(movie.getProductionCompanies());
        insertMovieProductionCompany(movie.getId(), movie.getProductionCompanies());
        insertProductionCountry(movie.getProductionCountries());
        insertMovieProductionCountry(movie.getId(), movie.getProductionCountries());
        insertSpokenLanguage(movie.getSpokenLanguages());
        insertMovieSpokenLanguage(movie.getId(), movie.getSpokenLanguages());
    }


    /**
     * Insert genre list of a movie
     * @param id_movie Movie id to edit
     * @param listGenre List of genres to add
     */
    private void insertMovieGenre(long id_movie, List<Genre> listGenre){
        StringBuilder sb = new StringBuilder();
        sb.append("INSERT INTO moviegenre (id_movie, id_genre) VALUES ");

        for(int i = 0; i < listGenre.size(); i++){
            sb.append("(").append(Long.toString(id_movie)).append(", ")
                    .append(Integer.toString(listGenre.get(i).getId())).append(")");
            if(i < listGenre.size() - 1)
                sb.append(", ");
        }

        db.execSQL(sb.toString());
    }


    /**
     * Get all movies in database
     * @return movie list
     */
    public List<Movie> getListMovies(){
        List<Movie> listMovies = new ArrayList<>();

        db = mySQLiteOpenHelper.getReadableDatabase();

        String request =
                "SELECT id, title, overview, backdrop_path, poster_path, vote_average, " +
                "release_date, adult, budget, homepage, original_language, original_title, " +
                "popularity, revenue, runtime, status, tagline, video, vote_count FROM movie";

        Cursor cursor = db.rawQuery(request, null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()){
            listMovies.add(
                    new Movie.Builder()
                            .id(cursor.getLong(0))
                            .title(cursor.getString(1))
                            .overview(cursor.getString(2))
                            .backdropPath(cursor.getString(3))
                            .posterPath(cursor.getString(4))
                            .voteAverage(cursor.getFloat(5))
                            .releaseDate(cursor.getString(6))
                            .adult(cursor.getInt(7) == 1)
                            .budget(cursor.getLong(8))
                            .homepage(cursor.getString(9))
                            .originalLanguage(cursor.getString(10))
                            .originalTitle(cursor.getString(11))
                            .popularity(cursor.getLong(12))
                            .revenue(cursor.getLong(13))
                            .runtime(cursor.getInt(14))
                            .status(cursor.getString(15))
                            .tagline(cursor.getString(16))
                            .video(cursor.getInt(17) == 1)
                            .voteCount(cursor.getLong(18))
                            .build()
            );

            cursor.moveToNext();
        }

        cursor.close();

        for(int i = 0; i < listMovies.size(); i++){
            Movie movie = listMovies.get(i);
            movie.setGenresList(getMovieGenre(movie.getId()));
            movie.setProductionCompanies(getProductionCompanies(movie.getId()));
            movie.setProductionCountries(getProductionCountries(movie.getId()));
            movie.setSpokenLanguages(getSpokenLanguages(movie.getId()));
        }

        Collections.reverse(listMovies);

        return listMovies;
    }

    /**
     * Get all genres for a movie
     * @param id_movie Id of the movie
     * @return genre list of movie
     */
    private List<Genre> getMovieGenre(long id_movie){
        List<Genre> listMovieGenre = new ArrayList<>();

        db = mySQLiteOpenHelper.getReadableDatabase();

        Cursor cursor = db.rawQuery(
                "SELECT id_genre, name " +
                    "FROM moviegenre, genre " +
                    "WHERE id_genre = id " +
                    "AND id_movie = " + id_movie, null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()){
            listMovieGenre.add(
                    new Genre.Builder()
                            .id(cursor.getInt(0))
                            .name(cursor.getString(1))
                            .build());
            cursor.moveToNext();
        }

        cursor.close();
        return listMovieGenre;
    }


    /**
     * Get productions companies for a movie
     * @param id_movie Id of the movie
     * @return List of production companies
     */
    private List<ProductionCompanies> getProductionCompanies(long id_movie) {
        List<ProductionCompanies> listProductionCompanies = new ArrayList<>();

        Cursor cursor = db.rawQuery(
                "SELECT id, logo_path, name, origin_country " +
                    "FROM productioncompany, movieproductioncompany " +
                    "WHERE id_productioncompany = id " +
                    "AND id_movie = " + id_movie, null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            listProductionCompanies.add(
                    new ProductionCompanies.Builder()
                            .id(cursor.getInt(0))
                            .logo_path(cursor.getString(1))
                            .name(cursor.getString(2))
                            .origin_country(cursor.getString(3))
                            .build()
            );
            cursor.moveToNext();
        }

        cursor.close();
        return listProductionCompanies;
    }


    /**
     * Get productions countries for a movie
     * @param id_movie Id of the movie
     * @return List of production countries
     */
    private List<ProductionCountries> getProductionCountries(long id_movie) {
        List<ProductionCountries> productionCountriesList = new ArrayList<>();

        Cursor cursor = db.rawQuery(
                "SELECT iso, name " +
                    "FROM productioncountry, movieproductioncountry " +
                    "WHERE iso_productioncountry = iso " +
                    "AND id_movie = " + id_movie, null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            productionCountriesList.add(
                    new ProductionCountries.Builder()
                            .iso(cursor.getString(0))
                            .name(cursor.getString(1))
                            .build()
            );
            cursor.moveToNext();
        }

        cursor.close();
        return productionCountriesList;
    }


    /**
     * Get spoken languages for a movie
     * @param id_movie Id of the movie
     * @return List of spoken languages
     */
    private List<LanguagesMovie> getSpokenLanguages(long id_movie) {
        List<LanguagesMovie> languagesMovieList = new ArrayList<>();

        Cursor cursor = db.rawQuery(
                "SELECT iso, name " +
                    "FROM spokenlanguage, moviespokenlanguage " +
                    "WHERE iso_spokenlanguage = iso " +
                    "AND id_movie = " + id_movie, null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()){
            languagesMovieList.add(
                    new LanguagesMovie.Builder()
                            .iso(cursor.getString(0))
                            .name(cursor.getString(1))
                            .build()
            );
            cursor.moveToNext();
        }

        cursor.close();
        return languagesMovieList;
    }


    /**
     * Get a Movie with its id
     * @param id Id of movie
     * @return Movie
     */
    public Movie getMovieById(long id){
        db = mySQLiteOpenHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery(
                "SELECT id, title, overview, backdrop_path, poster_path, vote_average, " +
                    "release_date, adult, budget, homepage, original_language, original_title, " +
                    "popularity, revenue, runtime, status, tagline, video, vote_count "+
                    "FROM movie WHERE id = " + id, null);
        cursor.moveToFirst();

        Movie movie =  new Movie.Builder()
                .id(cursor.getLong(0))
                .title(cursor.getString(1))
                .overview(cursor.getString(2))
                .backdropPath(cursor.getString(3))
                .posterPath(cursor.getString(4))
                .voteAverage(cursor.getFloat(5))
                .releaseDate(cursor.getString(6))
                .adult(cursor.getInt(7) == 1)
                .budget(cursor.getLong(8))
                .homepage(cursor.getString(9))
                .originalLanguage(cursor.getString(10))
                .originalTitle(cursor.getString(11))
                .popularity(cursor.getLong(12))
                .revenue(cursor.getLong(13))
                .runtime(cursor.getInt(14))
                .status(cursor.getString(15))
                .tagline(cursor.getString(16))
                .video(cursor.getInt(17) == 1)
                .voteCount(cursor.getLong(18))
                .build();

        cursor.close();

        movie.setGenresList(getMovieGenre(id));
        movie.setProductionCompanies(getProductionCompanies(movie.getId()));
        movie.setProductionCountries(getProductionCountries(movie.getId()));
        movie.setSpokenLanguages(getSpokenLanguages(movie.getId()));

        return movie;
    }


    /**
     * Delete a movie from database
     * @param id_movie Id of the movie
     */
    public void deleteMovie(long id_movie){
        db = mySQLiteOpenHelper.getWritableDatabase();
        db.execSQL("DELETE FROM moviegenre WHERE id_movie = " + id_movie);
        db.execSQL("DELETE FROM movie WHERE id = " + id_movie);
        db.execSQL("DELETE FROM movieproductioncompany WHERE id_movie = " + id_movie);
        db.execSQL("DELETE FROM movieproductioncountry WHERE id_movie = " + id_movie);
        db.execSQL("DELETE FROM moviespokenlanguage WHERE id_movie = " + id_movie);
    }


    /**
     * Check if movie exist in database
     * @param movie_id Id of the movie
     * @return If exist return True else return False
     */
    public boolean movieExist(long movie_id){
        db = mySQLiteOpenHelper.getReadableDatabase();

        Cursor cursor =
                db.rawQuery("SELECT COUNT(*) FROM movie WHERE id = " + movie_id, null);
        cursor.moveToFirst();

        int count = cursor.getInt(0);

        cursor.close();

        return count > 0;
    }


    /**
     * Delete all added movies from database
     */
    public void cleanDatabase(){
        db = mySQLiteOpenHelper.getWritableDatabase();
        db.execSQL("DELETE FROM moviegenre");
        db.execSQL("DELETE FROM movie");
        db.execSQL("DELETE FROM movieproductioncompany");
        db.execSQL("DELETE FROM movieproductioncountry");
        db.execSQL("DELETE FROM moviespokenlanguage");
    }

    /**
     * Count number of movies
     * @return number of movies
     */
    public int countMovies(){
        db = mySQLiteOpenHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT COUNT(*) FROM movie", null);
        cursor.moveToFirst();

        int count = cursor.getInt(0);

        cursor.close();

        return count;
    }

    /**
     * Search movies by title
     * @param title Title or part of the title to search
     * @return All matches movies
     */
    public List<Movie> searchMovieByTitle(String title){
        List<Movie> listMovies = new ArrayList<>();

        db = mySQLiteOpenHelper.getReadableDatabase();

        Cursor cursor = db.rawQuery(
                "SELECT id, title, overview, backdrop_path, poster_path, vote_average, " +
                    "release_date, adult, budget, homepage, original_language, original_title, "+
                    "popularity, revenue, runtime, status, tagline, video, vote_count " +
                    "FROM movie " +
                    "WHERE title LIKE '%" + title + "%'", null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()){
            listMovies.add(
                    new Movie.Builder()
                            .id(cursor.getLong(0))
                            .title(cursor.getString(1))
                            .overview(cursor.getString(2))
                            .backdropPath(cursor.getString(3))
                            .posterPath(cursor.getString(4))
                            .voteAverage(cursor.getFloat(5))
                            .releaseDate(cursor.getString(6))
                            .adult(cursor.getInt(7) == 1)
                            .budget(cursor.getLong(8))
                            .homepage(cursor.getString(9))
                            .originalLanguage(cursor.getString(10))
                            .originalTitle(cursor.getString(11))
                            .popularity(cursor.getLong(12))
                            .revenue(cursor.getLong(13))
                            .runtime(cursor.getInt(14))
                            .status(cursor.getString(15))
                            .tagline(cursor.getString(16))
                            .video(cursor.getInt(17) == 1)
                            .voteCount(cursor.getLong(18))
                            .build()
            );

            cursor.moveToNext();
        }

        cursor.close();

        for(int i = 0; i < listMovies.size(); i++){
            Movie movie = listMovies.get(i);

            movie.setGenresList(getMovieGenre(listMovies.get(i).getId()));
            movie.setProductionCompanies(getProductionCompanies(movie.getId()));
            movie.setProductionCountries(getProductionCountries(movie.getId()));
            movie.setSpokenLanguages(getSpokenLanguages(movie.getId()));
        }

        Collections.reverse(listMovies);

        return listMovies;
    }


    /**
     * Get limited number of movies
     * @param number Number of results
     * @return List of movies with max size of number
     */
    public List<Movie> getLimitedNumberMovies(int number){
        List<Movie> listMovies = new ArrayList<>();

        db = mySQLiteOpenHelper.getReadableDatabase();

        Cursor cursor = db.rawQuery(
                "SELECT id, title, overview, backdrop_path, poster_path, vote_average, " +
                    "release_date, adult, budget, homepage, original_language, original_title, "+
                    "popularity, revenue, runtime, status, tagline, video, vote_count " +
                    "FROM movie " +
                    "ORDER BY release_date DESC " +
                    "LIMIT " + number, null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()){
            listMovies.add(
                    new Movie.Builder()
                            .id(cursor.getLong(0))
                            .title(cursor.getString(1))
                            .overview(cursor.getString(2))
                            .backdropPath(cursor.getString(3))
                            .posterPath(cursor.getString(4))
                            .voteAverage(cursor.getFloat(5))
                            .releaseDate(cursor.getString(6))
                            .adult(cursor.getInt(7) == 1)
                            .budget(cursor.getLong(8))
                            .homepage(cursor.getString(9))
                            .originalLanguage(cursor.getString(10))
                            .originalTitle(cursor.getString(11))
                            .popularity(cursor.getLong(12))
                            .revenue(cursor.getLong(13))
                            .runtime(cursor.getInt(14))
                            .status(cursor.getString(15))
                            .tagline(cursor.getString(16))
                            .video(cursor.getInt(17) == 1)
                            .voteCount(cursor.getLong(18))
                            .build()
            );

            cursor.moveToNext();
        }

        cursor.close();

        for(int i = 0; i < listMovies.size(); i++){
            Movie movie = listMovies.get(i);

            movie.setGenresList(getMovieGenre(listMovies.get(i).getId()));
            movie.setProductionCompanies(getProductionCompanies(movie.getId()));
            movie.setProductionCountries(getProductionCountries(movie.getId()));
            movie.setSpokenLanguages(getSpokenLanguages(movie.getId()));
        }

        Collections.reverse(listMovies);

        return listMovies;
    }

    /**
     * Insert genre in database if not already exist
     * @param genre genre to insert
     */
    public void insertGenreIfNotExist(Genre genre){
        db = mySQLiteOpenHelper.getWritableDatabase();

        db.execSQL("REPLACE INTO genre VALUES (" +
                genre.getId() + ", \"" +
                genre.getName() + "\")");
    }

    /**
     * Get genre with its id
     * @param id id of the genre to search
     * @return Genre
     */
    public Genre getGenreById(int id){
        db = mySQLiteOpenHelper.getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT id, name FROM genre WHERE id = " + id, null);

        cursor.moveToFirst();

        Genre genre = new Genre.Builder()
                .id(cursor.getInt(0))
                .name(cursor.getString(1))
                .build();

        cursor.close();

        return genre;
    }

    /**
     * Get all Genre from database
     * @return List of Genre
     */
    public List<Genre> getAllGenre(){
        List<Genre> genreList = new ArrayList<>();

        db = mySQLiteOpenHelper.getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT id, name FROM genre ORDER BY name", null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()){
            genreList.add(
                    new Genre.Builder()
                            .id(cursor.getInt(0))
                            .name(cursor.getString(1))
                            .build()
            );

            cursor.moveToNext();
        }

        cursor.close();

        return genreList;
    }

    /**
     * Count number of genre
     * @return number of genre
     */
    public int countGenre(){
        db = mySQLiteOpenHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT COUNT(*) FROM genre", null);
        cursor.moveToFirst();

        int count = cursor.getInt(0);

        cursor.close();

        return count;
    }

    /**
     * Insert production companies
     * @param productionCompanies All production companies to insert
     */
    private void insertProductionCompany(List<ProductionCompanies> productionCompanies){
        StringBuilder sb = new StringBuilder();
        sb.append("REPLACE INTO productioncompany (id, logo_path, name, origin_country) VALUES ");

        for (int i = 0; i < productionCompanies.size(); i++){
            ProductionCompanies company = productionCompanies.get(i);

            sb.append("(").append(company.getId()).append(",")
                .append("\"").append(company.getLogo_path()).append("\",")
                .append("\"").append(company.getName()).append("\",")
                .append("\"").append(company.getOrigin_country()).append("\")");

            if(i < productionCompanies.size() - 1){
                sb.append(", ");
            }
        }

        db.execSQL(sb.toString());
    }


    /**
     * Insert MovieProductionCompany
     * @param movie_id Id of the movie
     * @param productionCompanies list of production company
     */
    private void insertMovieProductionCompany(long movie_id, List<ProductionCompanies> productionCompanies){
        StringBuilder sb = new StringBuilder();
        sb.append("INSERT INTO movieproductioncompany (id_movie, id_productioncompany) VALUES ");

        for (int i = 0; i < productionCompanies.size(); i++) {
            sb.append("(").append(movie_id).append(",")
                .append(productionCompanies.get(i).getId()).append(")");

            if(i < productionCompanies.size() - 1){
                sb.append(", ");
            }
        }

        db.execSQL(sb.toString());
    }


    /**
     * Insert ProductionCountries
     * @param productionCountries production countries to insert
     */
    private void insertProductionCountry(List<ProductionCountries> productionCountries){
        StringBuilder sb = new StringBuilder();
        sb.append("REPLACE INTO productioncountry (iso, name) VALUES ");

        for (int i = 0; i < productionCountries.size(); i++) {
            sb.append("(\"").append(productionCountries.get(i).getIso()).append("\",")
                .append("\"").append(productionCountries.get(i).getName()).append("\")");

            if(i < productionCountries.size() - 1){
                sb.append(", ");
            }
        }

        db.execSQL(sb.toString());
    }


    /**
     * Insert movie production country
     * @param movie_id Id of the movie
     * @param productionCountries list of production country
     */
    private void insertMovieProductionCountry(long movie_id, List<ProductionCountries> productionCountries){
        StringBuilder sb = new StringBuilder();
        sb.append("INSERT INTO movieproductioncountry (id_movie, iso_productioncountry) VALUES ");

        for (int i = 0; i < productionCountries.size(); i++) {
            sb.append("(").append(movie_id).append(", ")
                .append("\"").append(productionCountries.get(i).getIso()).append("\")");

            if(i < productionCountries.size() - 1){
                sb.append(", ");
            }
        }

        db.execSQL(sb.toString());
    }


    /**
     * Insert spoken language
     * @param languagesMovies list of spoken language
     */
    private void insertSpokenLanguage(List<LanguagesMovie> languagesMovies){
        StringBuilder sb = new StringBuilder();
        sb.append("REPLACE INTO spokenlanguage (iso, name) VALUES ");

        for (int i = 0; i < languagesMovies.size(); i++) {
            sb.append("(\"").append(languagesMovies.get(i).getIso()).append("\",")
            .append("\"").append(languagesMovies.get(i).getName()).append("\")");

            if(i < languagesMovies.size() - 1){
                sb.append(", ");
            }
        }

        db.execSQL(sb.toString());
    }


    /**
     * Insert movie spoken language
     * @param movie_id id of the movie
     * @param languagesMovies List of language movie
     */
    private void insertMovieSpokenLanguage(long movie_id, List<LanguagesMovie> languagesMovies){
        StringBuilder sb = new StringBuilder();
        sb.append("INSERT INTO moviespokenlanguage (id_movie, iso_spokenlanguage) VALUES ");

        for (int i = 0; i < languagesMovies.size(); i++) {
            sb.append("(").append(movie_id).append(",")
                .append("\"").append(languagesMovies.get(i).getIso()).append("\")");

            if(i < languagesMovies.size() - 1){
                sb.append(", ");
            }
        }

        db.execSQL(sb.toString());
    }
}
