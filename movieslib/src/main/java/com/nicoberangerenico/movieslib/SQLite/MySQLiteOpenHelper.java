package com.nicoberangerenico.movieslib.SQLite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class MySQLiteOpenHelper extends SQLiteOpenHelper {

    // Create table movie if not exist
    private String createMovie =
            "CREATE TABLE IF NOT EXISTS movie (" +
            "id INTEGER PRIMARY KEY," +
            "title TEXT," +
            "overview TEXT," +
            "backdrop_path TEXT," +
            "poster_path TEXT," +
            "vote_average REAL," +
            "release_date TEXT," +
            "adult NUMERIC," +
            "budget INTEGER," +
            "homepage TEXT," +
            "original_language TEXT," +
            "original_title TEXT," +
            "popularity REAL," +
            "revenue INTEGER," +
            "runtime INTEGER," +
            "status TEXT," +
            "tagline TEXT," +
            "video NUMERIC," +
            "vote_count INTEGER);";


    // Create table genre if not exist
    private String createGenre =
            "CREATE TABLE IF NOT EXISTS genre (" +
            "id INTEGER PRIMARY KEY," +
            "name TEXT);";


    // Create table moviegenre if not exist
    private String createMovieGenre =
            "CREATE TABLE IF NOT EXISTS moviegenre (" +
            "id_movie INTEGER NOT NULL," +
            "id_genre INTEGER NOT NULL," +
            "PRIMARY KEY (id_movie,id_genre)," +
            "FOREIGN KEY(id_movie) REFERENCES movie(id)," +
            "FOREIGN KEY(id_genre) REFERENCES genre(id));";


    //Create table productioncompany if not exist
    private String createProductionCompany =
            "CREATE TABLE IF NOT EXISTS productioncompany (" +
            "id INTEGER PRIMARY KEY," +
            "logo_path TEXT," +
            "name TEXT," +
            "origin_country TEXT)";


    // Create table movieproductioncompany if not exist
    private String createMovieProductionCompany =
            "CREATE TABLE IF NOT EXISTS movieproductioncompany (" +
            "id_movie INTEGER NOT NULL," +
            "id_productioncompany INTEGER NOT NULL," +
            "PRIMARY KEY (id_movie, id_productioncompany)," +
            "FOREIGN KEY(id_movie) REFERENCES movie(id)," +
            "FOREIGN KEY(id_productioncompany) REFERENCES productioncompany(id));";


    // Create table productioncountry if not exist
    private String createProductionCountry =
            "CREATE TABLE IF NOT EXISTS productioncountry (" +
            "iso TEXT PRIMARY KEY," +
            "name TEXT);";


    // Create table movieproductioncountry if not exist
    private String createMovieProductionCountry =
            "CREATE TABLE IF NOT EXISTS movieproductioncountry (" +
            "id_movie INTEGER NOT NULL," +
            "iso_productioncountry TEXT NOT NULL," +
            "PRIMARY KEY (id_movie, iso_productioncountry)," +
            "FOREIGN KEY(id_movie) REFERENCES movie(id)," +
            "FOREIGN KEY(iso_productioncountry) REFERENCES productioncountry(iso));";


    // Create table spokenlanguage if not exist
    private String createSpokenLanguage =
            "CREATE TABLE IF NOT EXISTS spokenlanguage (" +
            "iso TEXT PRIMARY KEY," +
            "name TEXT);";


    // Create table moviespokenlanguage if not exist
    private String createMovieSpokenLanguage =
            "CREATE TABLE IF NOT EXISTS moviespokenlanguage (" +
            "id_movie INTEGER NOT NULL," +
            "iso_spokenlanguage TEXT NOT NULL," +
            "PRIMARY KEY (id_movie, iso_spokenlanguage)," +
            "FOREIGN KEY(id_movie) REFERENCES movie(id)," +
            "FOREIGN KEY(iso_spokenlanguage) REFERENCES spokenlanguage(iso));";



    MySQLiteOpenHelper(Context context, String database_name, int database_version) {
        super(context, database_name, null, database_version);
        onCreate(this.getWritableDatabase());
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(createMovie);
        db.execSQL(createGenre);
        db.execSQL(createMovieGenre);

        db.execSQL(createProductionCompany);
        db.execSQL(createMovieProductionCompany);
        db.execSQL(createProductionCountry);
        db.execSQL(createMovieProductionCountry);
        db.execSQL(createSpokenLanguage);
        db.execSQL(createMovieSpokenLanguage);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {}
}
